library(Biobase); library(limma); library(edge); library(ggplot2)
# loading data
con =url(
  "http://bowtie-bio.sourceforge.net/recount/ExpressionSets/bottomly_eset.RData"
)
load(file=con)
close(con)
mp = bottomly.eset
pdata=pData(mp)
edata=as.data.frame(exprs(mp))
fdata = fData(mp)
# filtering lowly expressed genes
# transforming data
edata <- log2(edata + 1)
edata <- edata[rowMeans(edata) > 10,]
mod <- model.matrix(~ pdata$strain)  # creates matrix based on dummies
fit <- lm.fit(mod, t(edata))  # fun. t is needed because rows should samples
# and col features
# lm.fit is the bareboned function of lm. i.e. you can create mutiple
# regresions. It is less time-consuming.
fit$coefficients[,5]
length(fit$coefficients)
# we can plot the coefficients
ggplot(mapping = aes(x = fit$coefficients[1,])) + geom_histogram +
  labs(y = "Intercept")
ggplot(mapping = aes(x = fit$coefficients[2,])) + geom_histogram() +
  labs(y = "Slope")
# it is possible to plot single residuals
ggplot(mapping = aes(x = seq_along(fit$residuals[,1]), fit$residuals[,1])) +
  geom_point()  # one of too many
# Example adjusting variables
# create the matrix and then launch fun. lm
mod2 <- model.matrix(~ pdata$strain + as.factor(pdata$lane.number))
fit2 <- lm.fit(mod2, t(edata))
fit2$coefficients[,1]
# we get 8 coefficients
# there are many ways to create multiple regression e.g lmFit, fitEdge
fit_limma <- lmFit(edata, mod2)
fit_limma$coefficients[1,]  # for this fun. col must be samples
