# Coursera and Rosalind files
<div style="text-align: justify">
This repository contains files and projects created for different Coursera courses and the platform Rosalind. This file shows starred files and projects of this repository. All scripts, if not otherwise within the file, were created by Stefano Camborda. 

***

## Rosalind

Rosalind is an online platform for learning and practicing bioinformatics through computer programming and problem solving created by the University of California. With time, this repository will show my answers for given challenges.

1. Python Village [6/6]
2. Bioinformatics Stronghold [14/104]
3. Bioinformatics Armory [...]
4. Bioinformatics Textbook Track [...]
5. Algorithmic Heights [...]

***
## Coursera

### Genomic data science

1. Homeworks from module _Algorithms for DNA Sequencing_
   
   - [Homework 1](https://gitlab.com/camborda.s/coursera/blob/master/python/AlgorithmsDNA/week1/homework.py): creates reverse complement strands, implements algorithm of naive exact matching (also with an option of 2 mismatches).
   - [Homework 2](https://gitlab.com/camborda.s/coursera/blob/master/python/AlgorithmsDNA/week2/homework.py): implements versions of the naive exact matching and Boyer-Moore algorithms that counts and returns numbers of character combinations and alignments.
   - [Homework 3](https://gitlab.com/camborda.s/coursera/blob/master/python/AlgorithmsDNA/week3/homework.py): counts and returns the edit distance with fewest value between a sequence and a reference genome (excerpts). Also, returns the numbers of overlapping pairs of reads.
   - [Homework 4](https://gitlab.com/camborda.s/coursera/blob/master/python/AlgorithmsDNA/week4/homework.py): returns shortest or a candidate for common superstring of given set of strings.
  
    >_One goal is to merge homeworks and create a small module with specific functions for this portfolio._

2. [Homework](https://gitlab.com/camborda.s/coursera/blob/master/python/Week4_Final/final.py) from module _Python for Genomic Data Science_
   - Opens multi-FASTA file, returns numbers of records, creates complementary strand for given sequence, prints longest and shortest read, and checks for Open Reading Frame (ORF).

### Statistics with R

1. [A html file](https://gitlab.com/camborda.s/coursera/blob/master/StatisticsR/project01/intro_data_prob_project.html) from a [.Rmd file](https://gitlab.com/camborda.s/coursera/blob/master/StatisticsR/project01/intro_data_prob_project.Rmd) that explores the 2013 data of the Behavioral Risk Factor Surveillance System (BRFSS) and shows descriptive analyses regarding mental health.
</div>