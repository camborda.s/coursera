---
title: "Finding IMDb ratings from Rotten Tomatoes"
subtitle: "Modeling and prediction for movies"
output: 
  html_document: 
    fig_height: 4
    highlight: pygments
    theme: spacelab
---

### Setup

#### Load packages and data

```{r load-packages-data, message = FALSE}
library(ggplot2)  # user-friendly plotting
library(cowplot)  # for multiple plots
# Setting classic theme for ggplot2
theme_set(theme_bw())
library(tidyverse)  # tidydata
library(statsr)  # for analyses
library(GGally)  # for correlation plots
library(knitr); library(xtable); library(kableExtra)# for tables
load("movies.Rdata")  # data
```

* * *

## Part 1: Data

<div style="text-align: justify">
The data obtained for this study contained 651 randomly selected movies produced before 2016. The owners of the data explained that random sampling was used, meaning that the choice of the movies was unbiased. In this scenario, the study is observational since there is no treatment (control groups) involved and thus, making causal conclusions based on thesample is not recommendend. However, it provides associations that can be helpful for later experiments
</div>
<a name="script:trans"></a>
```{r transformation}
# checking for no character class
nonCharacterList <- names(movies)[sapply(movies,class) != "character"]
# creating new movie dataframe
DataMovie <- movies %>% select(nonCharacterList)
rm(nonCharacterList)  # cleaning workingspace
# Separating 'big 6'
DataMovie$studio <- ifelse(grepl("Warner|Paramount|Sony|Universal|Disney|Columbia",
                                 DataMovie$studio), "Yes", "No") %>% factor()
colnames(DataMovie)[5] <- "bigStudio"
# genre Art House & International into group 'Other' for simplicity
DataMovie$genre <- DataMovie$genre %>% fct_collapse(Other = c("Other", "Art House & International"))
# create new variable for wining oscar (actor OR actress)
DataMovie[,24] <- select(DataMovie) %>%  # to avoid complex names
  mutate(oscarActWin = ifelse(DataMovie$best_actor_win == "yes", "yes",  # if actor wins 
                       ifelse(DataMovie$best_actress_win == "yes", "yes", "no")))  # if actress wins
DataMovie$oscarActWin <- as.factor(DataMovie$oscarActWin)  # as factor
# for simplicity, no months nor days
DataMovie <- DataMovie %>% select(-thtr_rel_month, -thtr_rel_day, -dvd_rel_month, -dvd_rel_day,
                                  -imdb_num_votes,  # nothing imdb-related
                                  # only usingoscarActWim
                                  -best_actor_win, -best_actress_win) %>% na.omit()
```

* * *

## Part 2: Research question
<div style="text-align: justify">
The main goal of this research was to build a multilinear regression model that can predict the IMDb rating from Rotten Tomatoes-based variables. The main question of this analysis is:

> Which Rotten Tomatoes-related variables are needed to predict its IMDb rating?

Thus, this research focused in the interaction between Rotten Tomatoes-variables and *imdb_rating*. The model should predict a $\hat{y}_{imdb\_rating}$ as accurate as the original values. After data curation (this work is based on movies with complete variable information) and selection (See <a href="#script:trans">Script</a>), `r dim(DataMovie)[1]` movies between the years `r min(DataMovie$thtr_rel_year)` and `r max(DataMovie$thtr_rel_year)` were included from the original sample. The following 17 variables were used for the analysis:

- *title_type*: Type of movie (Documentary, Feature Film, TV Movie)
- *genre*: Genre of movie (Action & Adventure, Comedy, Documentary, Drama, Horror, Mystery & Suspense, Other)
- *runtime*: Runtime of movie (in minutes).
- *mpaa_rating*: MPAA rating of the movie (G, PG, PG-13, R, Unrated)
- *bigStudio*: If movie was produced in one of the 6 major film studios. (Warner, Paramount, Sony, Universal, Disney, Columbia).
- *thtr_rel_year*: Year the movie is released in theaters
- *dvd_rel_year*: Year the movie is released on DVD
- *imdb_rating*: Rating on IMDb
- *critics_rating*: Categorical variable for critics rating on Rotten Tomatoes (Certified Fresh, Fresh, Rotten)
- *critics_score*: Critics score on Rotten Tomatoes
- *audience_rating*: Categorical variable for audience rating on Rotten Tomatoes (Spilled, Upright)
- *audience_score*: Audience score on Rotten Tomatoes
- *best_pic_nom*: Whether or not the movie was nominated for a best picture Oscar (no, yes)
- *best_pic_win*: Whether or not the movie won a best picture Oscar (no, yes)
- *best_dir_win*: Whether or not the director of the movie ever won an Oscar (no, yes) – not that this is not necessarily whether the director won an Oscar for the given movie
- *oscarAct_won*: Whether or not at least one main actor or actress in the movie ever won an Oscar (no, yes) - note that this is not necessarily whether the actor won an Oscar for their role in the given movie
- *top200_box*: Whether or not the movie is in the Top 200 Box Office list on BoxOfficeMojo (no, yes)
</div>

* * *

## Part 3: Exploratory data analysis
<div style="text-align: justify">
Some variables appeared to have the shape of a normal distribution. e.g *runtime* and *imdb_rating*. This indicates that most movies from the sample had almost the same length and high IDMB ratings. The rest of the variables did not have constant variation. One feature of this plot was the high positive correlation of the variables *critics_score* and *audience_score*. This was considered when building the multilinear model.
<a name="fig:01"></a>
```{r fig01, fig.align='center', warning=FALSE, fig.height=7, fig.width=12, fig.cap='Figure 1. Plots of the  numerical variable coming from dataframe *DataMovie*. Each variable illustrates its distribution and correlations with the other variables. Scatterplots are also included. Correlation coefficients $(r)$ are represented by the values in the upper part of the figure.'}
dataNum <-DataMovie[sapply(DataMovie, class) != "factor"]
# distribution and correlation of numerical variables
ggpairs(dataNum, progress = F, lower = list(continuous = wrap("points",
                                            alpha =0.3, size = 0.5)))  # reduce size and alpha of points
```

```{r summary}
# table summary
summaryTab <- matrix(nrow = 4, ncol = 6)
summaryTab[1,] <- sapply(dataNum, mean)  # mean
summaryTab[2,] <- sapply(dataNum, median)  # median
summaryTab[3,] <- sapply(dataNum, sd)  #sd
summaryTab[4,] <- sapply(dataNum, IQR)  # IQR
colnames(summaryTab) <- names(dataNum)  # assigning colnames
rownames(summaryTab) <- c("mean", "median", "sd", "IRQ")  # assigning rownames
# print data in a presentable way
kable(summaryTab, caption = "Table 1. Summary statistics for the numerical variables") %>%
  kable_styling(full_width = F, bootstrap_options = "striped")  # striped rows, small table
```
It is normal for movies to last around 90 minutes. This is supported by the mean and standard deviation ($sd$) of *runtime*. As stated before, variables such *critics_score* and *audience_score* were not constant and had wide ranges. High interquartile ranges ($IRQ$) in the summary supports this statement.
<a name="fig:02"></a>
```{r fig02, fig.align='center', fig.height=6, fig.width=10, fig.cap='Figure 2. Plot of the first nine categorical variables'}
namesCat <- names(DataMovie)[sapply(DataMovie,class) == "factor"]  # only factors
plot_list =list()  # list of plots
# loop for plots
for (id in namesCat){
  if (id != "genre"){  # too large
  if (id != "mpaa_rating"){  # too large
  p <- ggplot(DataMovie, aes_string(id, fill = id)) +
    geom_bar(fill = "steelblue",position = "dodge2", colour = "Black") +
    theme(legend.position = "none")
  plot_list[[id]] <- p  # addint to the list
}}}
# plotting first 9 variables
plot_grid(plotlist = plot_list, align = "hv", ncol = 3 )
```
Some variables showed dominance such as *title_type* or *top200_box* in one of their levels. It was expected that most of the films were not TV movies nor documentaries. Also, only a small part of film gets a nomination for the Oscars, and thus, the relative value for nominees will be always low. The rest of the variables like *audience_rating* had traits of homogeneity.

<a name="fig:03"></a>
```{r fig03, fig.height=4.5, fig.width=10, fig.align='center', fig.cap='Figure 3. Plot for the categorical variables *gender* and *mpaa_rating*'}
# adding rest
plot_list[["genre"]] <- ggplot(DataMovie, aes(genre)) +
  geom_bar(fill = "steelblue", colour = "black") +
  scale_x_discrete(labels=function(x){sub("\\s", "\n", x)})
plot_list[["mpaa_rating"]] <- ggplot(DataMovie, aes(mpaa_rating)) +
  geom_bar(fill = "steelblue", colour = "black") +
  scale_x_discrete(labels=function(x){sub("\\s", "\n", x)})
# two plots
plot_grid(plot_list$genre, plot_list$mpaa_rating, nrow = 2)
```
The sample had mostly drama films. The lack of genres such as animation or Science fiction may alter the importance of this variable. Otherwise, the rest of the levels appear somewhat homogeneous. Most of the films were rated R and only a few NC-17. This distribution resemble the real distribution ([Reference](https://plot.ly/~Dreamshot/190/distribution-of-releases-by-mpaa-rating.embed)) </div>

* * *

## Part 4: Modeling

<div style="text-align: justify">
The model selection for this research was based on the adjusted R-squared ($R^{2}_{adj}$) forward selection. In each step, $R^{2}_{adj}$ of all possible combinations are calculated and the variable with the highest $R^{2}_{adj}$ is chosen to be included in the next step. This procedure is repeated until no variable shows a higher $R^{2}_{adj}$ than the last proposed model.

```{r modeling001}
# Step 01
step01 <- as.data.frame(matrix(nrow = 1, ncol = 16))  # create dataframe
names(step01) <- names(DataMovie)[-8]  # assigning names without imdb rating
# loop to make forward based on R^2
for (i in seq_along(step01)){  # creating range
  id <- names(step01)[i]  # assigning variable name 'id'
  form <- as.formula(paste("imdb_rating ~", id))  # defining formula
  adjR <- summary(lm(form, DataMovie))$adj.r.squared  # adding R^2 to matrix
  step01[1, i] <- round(adjR, 4)  # rounding
}
# Table
kable(step01[c(1:9)],
      caption = "Table 2. $R^{2}_{adj}$ of all candidate variables for forward model selection") %>%
  kable_styling(bootstrap_options = "striped"); kable(step01[c(10:16)]) %>%
  kable_styling(bootstrap_options = "striped") %>%
  footnote("The values are based on the linear model: *imdb_rating ~ candidate*" )
cat("New adj.R2:", names(step01)[max.col(step01)], "=", max(step01))  # highest value
```

```{r modeling002}
# Step 2
step02 <- as.data.frame(matrix(nrow = 1, ncol = 15))  # create dataframe
names(step02) <- names(DataMovie)[c(-8,-12)]  # only new names
# loop to make forward based on R^2
for (i in seq_along(step02)){  # creating range
  id <- names(step02)[i]  # assigning variable name 'id'
  form <- as.formula(paste("imdb_rating ~ audience_score +", id))  # defining formula
  adjR <- summary(lm(form, DataMovie))$adj.r.squared  # adding R^2 to matrix
  step02[1, i] <- round(adjR, 4)  # rounding
}
# Table
kable(step02[c(1:8)],
      caption = "Table 3. $R^{2}_{adj}$ of candidate variables for step 2 of forward model selection") %>%
  kable_styling(bootstrap_options = "striped"); kable(step02[c(9:15)]) %>%
  kable_styling(bootstrap_options = "striped") %>%
  footnote("The values are based on the linear model: *imdb_rating ~ audience_score + candidate*" )
cat("Last adj.R2:", max(step01), "\n",  # printing
  "Max adj.R2:",names(step02)[max.col(step02)],"=" ,max(step02))  # collinear
```
The script showed that the next variable was `r names(step02)[max.col(step02)]`. Nonetheless, <a href="#fig:01">Figure 1</a> showed a positive strong correlation between `r names(step02)[max.col(step02)]` and `r names(step01)[max.col(step01)]`. Collinearity invalidates the model and thus, `r names(step02)[max.col(step02)]` was not taken into consideration. The second highest $R^{2}_{adj}$ was chosen.
```{r modeling002b}
cat("Last adj.R2:", max(step01), "\n",  # printing
  "New max adj.R2:",names(step02)[max.col(step02[-9])], "=", max(step02[-9]))
```

```{r modelling003}
# Step 3
step03 <- as.data.frame(matrix(nrow = 1, ncol = 13))
names(step03) <- names(DataMovie)[c(-8,-12,-10,-2)]  # only new names, no colinears
# loop to make forward based on R^2
for (i in seq_along(step03)){  # creating range
  id <- names(step03)[i]  # assigning variable name 'id'
  form <- as.formula(paste("imdb_rating ~ audience_score +
                           genre +", id))  # defining formula
  adjR <- summary(lm(form, DataMovie))$adj.r.squared  # adding R^2 to matrix
  step03[1, i] <- round(adjR, 4)  # rounding
}
# Table
kable(step03[c(1:8)],
      caption = "Table 4. $R^{2}_{adj}$ of candidate variables for step 3 of forward model selection") %>%
  kable_styling(bootstrap_options = "striped"); kable(step03[c(9:13)]) %>%
  kable_styling(bootstrap_options = "striped") %>%
  footnote("The values are based on the linear model: *imdb_rating ~ audience_score + genre + candidate*
           No collinear variables are being taken into consideration")
cat("Last adj.R2:", max(step02[-9]), "\n",  # printing
  "Max adj.R2:",names(step03)[max.col(step03)], "=", max(step03))
```

```{r modelling004}
# Step 4
step04 <- as.data.frame(matrix(nrow = 1, ncol = 12))
names(step04) <- names(DataMovie)[c(-8,-12,-10,-2,-9)]  # only new names, no colinears
# loop to make forward based on R^2
for (i in seq_along(step04)){  # creating range
  id <- names(step04)[i]  # assigning variable name 'id'
  form <- as.formula(paste("imdb_rating ~ audience_score +
                           genre + critics_rating +", id))  # defining formula
  adjR <- summary(lm(form, DataMovie))$adj.r.squared  # adding R^2 to matrix
  step04[1, i] <- round(adjR, 4)  # rounding
}
# Table
kable(step04[c(1:7)],
      caption = "Table 5. $R^{2}_{adj}$ of candidate variables for step 4 of forward model selection") %>%
  kable_styling(bootstrap_options = "striped"); kable(step04[c(8:12)]) %>%
  kable_styling(bootstrap_options = "striped") %>%
  footnote(
"The values are based on the linear model: *imdb_rating ~ audience_score + genre + critics_rating + candidate*
No collinear variables are being taken into consideration" )
cat("Last adj.R2:", max(step03), "\n",  # printing
  "Max adj.R2:",names(step04)[max.col(step04)], "=", max(step04))
```

```{r modelling005}
step05 <- as.data.frame(matrix(nrow = 1, ncol = 11))
names(step05) <- names(DataMovie)[c(-8,-12,-10,-2,-9,-11)]  # only new names, no colinears
# loop to make forward based on R^2
for (i in seq_along(step05)){  # creating range
  id <- names(step05)[i]  # assigning variable name 'id'
  form <- as.formula(paste("imdb_rating ~ audience_score +
                           genre + critics_rating + audience_rating +", id))  # defining formula
  adjR <- summary(lm(form, DataMovie))$adj.r.squared  # adding R^2 to matrix
  step05[1, i] <- round(adjR, 4)  # rounding
}
# Table
kable(step05[c(1:6)],
      caption = "Table 6. $R^{2}_{adj}$ of candidate variables for step 5 of forward model selection") %>%
  kable_styling(bootstrap_options = "striped"); kable(step05[c(7:11)]) %>%
  kable_styling(bootstrap_options = "striped") %>% footnote("The values are based on the linear model:
*imdb_rating ~ audience_score + genre + critics_rating + audience_rating + candidate*
No collinear variables are being taken into consideration" )
cat("Last adj.R2:", max(step04), "\n",  # printing
  "Max adj.R2:",names(step05)[max.col(step05)], "=", max(step05))
```

```{r modelling006}
step06 <- as.data.frame(matrix(nrow = 1, ncol = 10))
names(step06) <- names(DataMovie)[c(-8,-12,-10,-2,-9,-11,-3)]   # only new names, no colinears
# loop to make forward based on R^2
for (i in seq_along(step06)){  # creating range
  id <- names(step06)[i]  # assigning variable name 'id'
  form <- as.formula(paste("imdb_rating ~ audience_score +
                           genre + critics_rating + audience_rating +
                           runtime + ", id))  # defining formula
  adjR <- summary(lm(form, DataMovie))$adj.r.squared  # adding R^2 to matrix
  step06[1, i] <- round(adjR, 4)  # rounding
}
# Table
kable(step06[c(1:5)],
      caption = "Table 7. $R^{2}_{adj}$ of candidate variables for step 6 of forward model selection") %>%
  kable_styling(bootstrap_options = "striped"); kable(step06[c(6:10)]) %>%
  kable_styling(bootstrap_options = "striped") %>% footnote("The values are based on the linear model:
*imdb_rating ~ audience_score + genre + critics_rating + audience_rating + runtime + candidate*
No collinear variables are being taken into consideration")
cat("Last adj.R2:", max(step05), "\n",  # printing
  "Max adj.R2:",names(step06)[max.col(step06)], "=", max(step06))
```
```{r modelling007}
step07 <- as.data.frame(matrix(nrow = 1, ncol = 9))
names(step07) <- names(DataMovie)[c(-8,-12,-10,-2,-9,-11,-3,-5)]   # only new names, no colinears
# loop to make forward based on R^2
for (i in seq_along(step07)){  # creating range
  id <- names(step07)[i]  # assigning variable name 'id'
  form <- as.formula(paste("imdb_rating ~ audience_score +
                           genre + critics_rating + audience_rating +
                           runtime + bigStudio + ", id))  # defining formula
  adjR <- summary(lm(form, DataMovie))$adj.r.squared  # adding R^2 to matrix
  step07[1, i] <- round(adjR, 4)  # rounding
}
# Table
kable(step07[c(1:5)],
      caption = "Table 8. $R^{2}_{adj}$ of candidate variables for step 7 of forward model selection") %>%
  kable_styling(bootstrap_options = "striped"); kable(step07[c(6:9)]) %>%
  kable_styling(bootstrap_options = "striped") %>% footnote("The values are based on the linear model:
*imdb_rating ~ audience_score + gender + critics_rating + audience_rating + runtime + bigStudio + candidate*
No collinear variables are being taken into consideration")
cat("Last adj.R2:", max(step06), "\n",  # printing
  "Max adj.R2:",names(step07)[max.col(step07)], "=", max(step07))
```
```{r modelling008}
step08 <- as.data.frame(matrix(nrow = 1, ncol = 8))
names(step08) <- names(DataMovie)[c(-8,-12,-10,-2,-9,-11,-3,-5,-1)]   # only new names, no colinears
# loop to make forward based on R^2
for (i in seq_along(step08)){  # creating range
  id <- names(step08)[i]  # assigning variable name 'id'
  form <- as.formula(paste("imdb_rating ~ audience_score +
                           genre + critics_rating + audience_rating +
                           runtime + bigStudio + title_type +", id))  # defining formula
  adjR <- summary(lm(form, DataMovie))$adj.r.squared  # adding R^2 to matrix
  step08[1, i] <- round(adjR, 4)  # rounding
}
# Table
kable(step08[c(1:8)],
      caption = "Table 8. $R^{2}_{adj}$ of candidate variables for step 7 of forward model selection") %>%
  kable_styling(bootstrap_options = "striped") %>% footnote("The values are based on the linear model:
*imdb_rating~audience_score+gender+critics_rating+audience_rating+runtime+ bigStudio + title_type + candidate*
No collinear variables are being taken into consideration")
cat("Last adj.R2:", max(step07), "\n",  # printing
  "Max adj.R2:",names(step08)[max.col(step08)], "=", max(step08))
```
Because $R^{2}_{adj}$ cannot increase further, this model was chosen. An overview of the model with corresponding coefficients is needed to check the quality of a multilinear regression model.
<a name="tab:final"></a>
```{r finalModel}
# Table
finalModel <- lm(imdb_rating ~ audience_score + genre + critics_rating + audience_rating +
                           runtime + bigStudio + title_type, DataMovie)
a <- finalModel$coefficients  # getting coefficients for eq
a <- round(a,4)  # rounding for eq LaTeX
summary(finalModel)
```
The code above shows the summary of the model. Because the variable *gender* had at least one significant variable, it was included in the final model. Most of the variables were significant ($\text{p-value} < 0.05$). However, those which were not, stayed in the model since the objective of this research is to predict $\hat{y}_{imdb\_rating}$ as accurate as possible. i.e. retain the model with the highest $R^{2}_{adj}$ while maintaining model simplicity.

After the taking all the variables into consideration and selecting only the necessary variables, the final model is presented as:

> $$\hat{y}_{imdb\_rating} = `r paste(a[1],"+", a[2], paste0("x_{", sub("_", "\\\\_", names(a)[2]), "}"), a[3], paste0("x_{", names(a)[3], "}+"),a[4], paste0("x_{", names(a)[4], "}\\\\"), a[5], paste0("x_{", names(a)[5], "}+"), a[6], paste0("x_{", names(a)[6], "}+"), a[7], paste0("x_{", names(a)[7], "}+"),a[8], paste0("x_{", names(a)[8], "}+\\\\"), a[9], paste0("x_{",sub("& Performing Arts", "", names(a)[9]), "}+"), a[10], paste0("x_{", sub("\\& Suspense", "", names(a)[10]), "}"), a[11], paste0("x_{", sub(" Fiction & Fantasy","\\\\_Fiction", names(a)[11]), "}"), a[12], paste0("x_{", sub("_", "\\\\_", names(a)[12]), "}\\\\"), a[13], paste0("x_{", sub("_", "\\\\_", names(a)[13]), "}"), a[14], paste0("x_{", sub("_", "\\\\_", names(a)[14]), "}+"), a[15], paste0("x_{", names(a)[15], "}+"), a[16], paste0("x_{", names(a)[16], "}\\\\"), a[17], paste0("x_{", sub("title_typeFeature Film", "title\\\\_typeFeatureFilm", names(a)[17]), "}"), a[18], paste0("x_{", sub("title_typeTV Movie", "title\\\\_typeTV", names(a)[18]), "}"))`$$

      
Each coefficient represent how much a specific characteristic adds or reduces $\hat{y}_{imdb\_rating}$. For instance, if the genre of a movie is *Drama*, it would increase `r finalModel$coefficients[7]` to the estimation. Multiple genres are not allowed in this model. For each categorical variable, a standard level is assigned to the model, which implies that these particular levels are represented by the intercept. e.g. this multilinear model has as bases *genreAction*, *critics_ratingFreshCertified*, *audience_ratingSpilled*, *bigStudioNo*, *title_typeDocumentary*. Another example is the numerical variable  *audience_score* that add for each point `r finalModel$coefficients[2]` to the model. (See <a href="#tab:final">Summary</a>).

<a name="fig:04"></a>
```{r fig:04, fig.align='center', fig.height=5, fig.width=10, fig.cap='Figure 4. Model diagnostics. Each plot demonstrates the behaviour of the residuals regarding its distribution, variability, independence and linearity with numerical variables from the multilinear model'}
plot_list =list()  # reset list
# normality (some outliers, but mostly normal)
plot_list[["norm1"]] <- ggplot(mapping = aes(x = finalModel$residuals)) +
  geom_histogram(bins = 20) + labs(x = "Residual distribution")
plot_list[["norm2"]] <- ggplot(mapping = aes(sample = finalModel$residuals)) +
  geom_qq_line(colour = "blue") + geom_qq(alpha = 0.5) + labs(x = "Normal theoretical quantiles",
                                                              y = "Sample quantiles")
# constant variability of residuals
plot_list[["consVar1"]] <- ggplot(mapping = aes(finalModel$fitted.values, finalModel$residuals)) +
  geom_jitter(alpha = 0.7) + geom_smooth(method = "lm", se = F) + labs(y = "Model residuals",
                                                                       x = "Model predictions ")
# linearity with variables
plot_list[["line1"]] <- ggplot(mapping = aes(DataMovie$runtime, finalModel$residuals)) +
  geom_jitter(alpha = 0.7) + geom_smooth(method = "lm", se = F) + labs(y = "Model residuals",
                                                                       x = "runtime")
plot_list[["line2"]] <- ggplot(mapping = aes(DataMovie$audience_score, finalModel$residuals)) +
  geom_jitter(alpha = 0.7) + geom_smooth(method = "lm", se = F) + labs(y = "Model residuals",
                                                                       x = "audience_score")
# independence (no timeline)
plot_list[["indep"]] <- ggplot(mapping = aes(seq_along(finalModel$residuals), finalModel$residuals)) +
  geom_point(alpha = 0.7) + geom_smooth(method = "lm", se = F) + labs(x = "Index", y = " Model residuals")
plot_grid(plotlist = plot_list)  # plotting
```
The plots above demonstrate that the residuals have a normal distribution with some negative outliers. Nonetheless, the residuals maintain normality most of the time (Plot 1 and 2). The lack of low rated movies in the sample could be the explanation for this issue. In addition, the third plot depicts constant variability for the residuals since they remain constant around the 0 y-value. Plot 4 and 5 illustrate the linearity between the model residuals and the numerical variables, *runtime* and *audience_score*. Both variables show no trends i.e. linearity remains constant and unbiased. Finally, the index plot show no trends. i.e. time of the year was not important for the rating. These requirements are needed to validate a multilinear model, which in this case, were all met.
</div>

***

## Part 5: Prediction

<div style="text-align: justify">
In order to check the fit of the final model, five random movies were chosen with the website [Random List](https://www.randomlists.com/random-movies) in order to predict its IMDb ratings. Only movies up to the year 2016 which are not in the sample were taken into consideration. The movies chosen were:

1. Rogue One: A Star Wars Story (2016).
2. Boyhood (2014).
3. How the Grinch stole Christmas (2000).
4. The Rocky Horror Picture Show (1975). 
5. Spice World (1998). 

The information of the movies was obtained the 26th of December 2019 at the corresponding websites of IMDb and Rotten Tomatoes. [Appendix A] contains the original URLs from both website for each movie. For each movie a prediction interval was built. (See <a href="#fig:05">Figure 5</a>)
```{r movies}
movieTestList = list()  # list of movies
# Rogue One: A Star Wars Story (7.8)
movieTestList[[1]] <- data.frame(audience_score = 86, genre = "Action & Adventure",
                                 critics_rating = "Certified Fresh",
                                 audience_rating = "Upright", runtime = 133, imdb = 7.8,
                                 bigStudio = "Yes", title_type = "Feature Film",
                                 name = "Rogue One:\nA Star Wars Story")
# Boyhood (7.9)
movieTestList[[2]] <- data.frame(audience_score = 80, genre = "Drama",
                                 critics_rating = "Certified Fresh",
                                 audience_rating = "Upright", runtime = 165, imdb = 7.9,
                                 bigStudio = "No", title_type = "Feature Film",
                                 name = "Boyhood")
# How the Grinch stole Christmas (6.1)
movieTestList[[3]] <- data.frame(audience_score = 56, genre = "Comedy",
                                 critics_rating = "Rotten",
                                 bigStudio = "Yes", title_type = "Feature Film",
                                 audience_rating = "Spilled", runtime = 105, imdb = 6.1,
                                 name = "How the Grinch\nstole Christmas")
# The Rocky Horror Picture Show (7.4) 
movieTestList[[4]] <- data.frame(audience_score = 85, genre = "Comedy",
                                 critics_rating = "Fresh",
                                 bigStudio = "No", title_type = "Feature Film",
                                 audience_rating = "Upright", runtime = 100, imdb = 7.4,
                                 name = "The Rocky Horrow\nPicture Show")
# Spice World (3.5)
movieTestList[[5]] <- data.frame(audience_score = 35, genre = "Comedy",
                                 critics_rating = "Rotten",
                                 audience_rating = "Spilled", runtime = 92, imdb = 3.5,
                                 bigStudio = "Yes", title_type = "Feature Film",
                                 name = "Spice World")

```
<a name="fig:05"></a>
```{r fig05, fig.align='center', fig.height=4, fig.width=8, fig.cap='Figure 5. Prediction intervals for the 5 movies randomly chosen. The width of each line represent the scale of its corresponding interval. Black dots show the real IMDb rating while blue dots the fitted values for the final model'}
predMovies <- data.frame() # creating df for plots
for (id in c(1:5)){  # loop to spare space
  b <- data.frame(predict(finalModel, movieTestList[[id]], interval = "prediction"),
                  imdb = movieTestList[[id]]$imdb,  # adding score
                  row.names = movieTestList[[id]]$name)  #adding names
  predMovies <- rbind(predMovies, b)  # binding rows
  }
# Plotting
ggplot(predMovies, aes(y = row.names(predMovies), xmax = predMovies$upr, xmin = predMovies$lwr)) +
  geom_errorbarh() +  # addinng error bars
  geom_point(aes(x = imdb), size = 2) + labs(x = "IMDb rating", y = "Random Movies") +  # prediction
  geom_point(aes(x = fit), size = 2, colour = "steelblue")  # original
```
Most of the fitted values were in range of their corresponding prediction intervals. Such interval (not to be confused with confidence interval) predicts the value of a new observation based on the final linear model. In this example, taking the movie _Rogue one_, we could predict with a 95% confidence that a movie with such characteristics would have a range from `r predMovies$lwr[1]` up to `r predMovies$upr[1]` in the future.

Only the IMDb rating of _Spice World_ was overestimated by `r predMovies$fit[5] - predMovies$imdb[5]`. This can be explained due to the large amount of movies which obtained high IMDb ratings in the sample. However, the final lineal model is able to obtain values which are in range of the intervals and also near the original values, as long the overall ratings are not low. 

* * *

## Part 6: Conclusion

 - This model was based on the non-IMDb variables *imdb_rating* , *audience_score*, *genre*, *critics_rating*, *audience_rating*, *runtime*, *bigStudio* and *title_type*.
 - Mystery & Suspense movies will on average increase the rating while animation movies decrease the rating. This is likely due to the low amount of animated movies in the sample, which in their majority, did not had high ratings.
 - Documentary Films will have on average better results than the rest of movie types.
 - The values of "good" variables such *Fresh* or *Certifed Fresh* are most likely negative in order to stabilize the model and avoid overestimates.
 - Also, movies from major studios have on overall a better rating.
 - Due to the lack of low rated movies in the sample, this model will most likely work with movies with at least above average rating.
 - In order to improve the prediction of $\hat{y}_{imdb\_rating}$, a better rating distribution of films in the sample is needed.
 
* * *

## Appendix A
### Urls

1. Rogue One: A Star Wars Story (2016). [IMDb](https://www.imdb.com/title/tt3748528/?ref_=nv_sr_srsg_11), [Rotten Tomatoes](https://www.rottentomatoes.com/m/rogue_one_a_star_wars_story)
2. Boyhood (2014). [IMDb](https://www.imdb.com/title/tt1065073/?ref_=nv_sr_srsg_0), [Rotten Tomatoes](https://www.rottentomatoes.com/m/boyhood)
3. How the Grinch stole Christmas (2000). [IMDb](https://www.imdb.com/title/tt0170016/?ref_=ttls_li_tt), [Rotten Tomatoes](https://www.rottentomatoes.com/m/how_the_grinch_stole_christmas)
4. The Rocky Horror Picture Show (1975). [IMDb](https://www.imdb.com/title/tt0073629/?ref_=ttls_li_tt), [Rotten Tomatoes](https://www.rottentomatoes.com/m/rocky_horror_picture_show)
5. Spice World (1998). [IMDb](https://www.imdb.com/title/tt0120185/?ref_=kw_li_tt), [Rotten Tomatoes](https://www.rottentomatoes.com/m/spice_world)
</div>

***

### Session info
```{r session, echo=FALSE}
sessionInfo()
```

