"""
Given: A string s of length at most 200 letters and four integers a, b, c 
and d.

Return: The slice of this string from indices a through b and c through d (with
 space in between), inclusively. In other words, we should include elements 
 s[b] and s[d] in our slice.
"""
def indexStrings(ss,a,b,c,d):
    """ it has to include b and d, and thus +1"""
    first = ss[a:b+1]
    second = ss[c:d+1]
    text = "".join([first," ",second])
    return text
# test
# HumptyDumptysatonawallHumptyDumptyhadagreatfallAlltheKingshorsesandalltheKingsmenCouldntputHumptyDumptyinhisplaceagain.
# 22 27 97 102
ss="oPX1rwOeTmZsj8KfHiey5H3rXjCyclagrasOl45PNUL1Olra0NaDt0eSjowec9yfQztEs5lCdsUwOoMtrPf6fEn3G9mSscYCyoYBoOxI7Z5ati1LXFcmelBKn6P7JZVmhfN5XflavescensLC70YRpidBbz6JOAoH8RTbfgeLimRb."
print(indexStrings(ss,26, 34, 133, 142))