"""
Given: A string s of length at most 10000 letters.

Return: The number of occurrences of each word in s, where words are separated
by spaces. Words are case-sensitive, and the lines in the output can be in any
order.
"""
from pathlib import Path
DirPath = Path.cwd().joinpath("coursera").joinpath("rosalind").joinpath("01_pythonVillage")
FileName = "ini6_comp02.txt"
FilePath = DirPath.joinpath(FileName)

def openFileString(file):
    """ creates a string from lines of file """
    # open file
    string = str()
    f = open(FilePath,"r")
    for line in f:
        line = line.rstrip()
        newLine = "".join([line])
        # appending to string, string comes always first
        string = " ".join([string,newLine])
    f.close()
    return string

def OccuInString(file):
    """ Returns keys and values for each word in given file"""
    string = openFileString(file)
    # Defaults
    dictWords = dict()
    for word in string.split(" "):
        # appending to dictionary
        dictWords[word] = 0
    # count words
    for word in string.split(" "):
        if word in dictWords.keys():
            dictWords[word] += 1
    try:
        del dictWords[""]
    except KeyError:
        pass
    for key, value in dictWords.items():
        print(key, value)
    return dictWords
    
a = OccuInString(FileName)
