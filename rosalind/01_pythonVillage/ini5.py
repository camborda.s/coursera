"""
Given: A file containing at most 1000 lines.

Return: A file containing all the even-numbered lines from the original file.
 Assume 1-based numbering of lines.
"""
from pathlib import Path
DirPath = Path.cwd().joinpath("coursera").joinpath("rosalind").joinpath("01_pythonVillage")
FileName = "ini5_comp01.txt"
FilePath = DirPath.joinpath(FileName)
def openWriteFile(file):
    """ creates a new file with even lines of file """
    # open file
    f = open(FilePath,"r")
    lineCount = 0
    # list of strings to be passed 
    ListStrings = list()
    for line in f:
        # give line a number
        lineCount += 1
        # evens
        if lineCount%2 == 0:
            ListStrings.append(line)
    f.close()
    f = open(DirPath.joinpath("ini5_comp02.txt"),"w+")
    for line in ListStrings:
        # lines do not need an empty string
        f.write(line)
    f.close()

openWriteFile(FileName)