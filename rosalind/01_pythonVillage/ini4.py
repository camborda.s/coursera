"""
Given: Two positive integers a and b (a<b<10000).

Return: The sum of all odd integers from a through b, inclusively.
"""

def sumOddInt(a,b):
    # empty list
    listAB = list()
    # create list of integers from a to b
    for i in range(a,b+1):
        # has to be odd
        if i%2 != 0:
            listAB.append(i)
    # sum list
    sumOfList = sum(listAB)
    return sumOfList

# test
a = sumOddInt(4716,8739)
print(a)
