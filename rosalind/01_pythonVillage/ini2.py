"""
Given: Two positive integers a and b, each less than 1000.
Return: The integer corresponding to the square of the hypotenuse
 of the right triangle whose legs have lengths a and b. 
"""
def squareHypotenuse(a,b):
    """a^2 + b^2 = c^2, c^2 is the square of the hypothenuse """
    c = a**2 + b**2
    return c
# test
print(squareHypotenuse(960,933))