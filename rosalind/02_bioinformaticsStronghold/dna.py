"""
Given: A DNA string s of length at most 1000 nt.

Return: Four integers (separated by spaces) counting the respective number of
times that the symbols 'A', 'C', 'G', and 'T' occur in s.]
"""
from pathlib import Path
DirName = Path.cwd().joinpath("coursera").joinpath("rosalind").\
    joinpath("02_bioinformaticsStronghold")
FileName = "dna01.txt"
FilePath = DirName.joinpath(FileName)


def openFileString(file):
    """ creates a string from file """
    string = ""
    f = open(FilePath, "r")
    for line in f:
        line = line.rstrip()
        newLine = "".join([line])
        string = " ".join([string, newLine])
    f.close()
    return string


def countNuc(file):
    """ returns amount of A C G T occurred in file """
    string = openFileString(FilePath)
    # create dictionary
    nucleotide = str()
    dictNuc = {"A": 0, "C": 0, "G": 0, "T": 0}
    for i in range(len(string)):
        nucleotide = string[i]
        if nucleotide in dictNuc.keys():
            dictNuc[nucleotide] += 1
    return dictNuc["A"], dictNuc["C"], dictNuc["G"], dictNuc["T"]


def writeFile(file):
    a, c, g, t = countNuc(file)
    listNuc = [a, c, g, t]
    for i in range(len(listNuc)):
        nucleotide = listNuc[i]
        listNuc[i] = str(nucleotide)
    line = " ".join(listNuc)
    f = open(DirName.joinpath("dna02.txt"), "w+")
    f.write(line)
    f.close()


writeFile(FilePath)
