"""
Given: A DNA string t having length at most 1000 nt.
Return: The transcribed RNA string of t.
"""
from pathlib import Path
DirName = Path.cwd().joinpath("coursera").joinpath("rosalind").\
    joinpath("02_bioinformaticsStronghold")
FileName = "rna01.txt"
FilePath = DirName.joinpath(FileName)


def openFileString(file):
    """ creates a string from file """
    string = ""
    f = open(FilePath, "r")
    for line in f:
        line = line.rstrip()
        newLine = "".join([line])
        string = " ".join([string, newLine])
    f.close()
    return string


def UreplaceT(file):
    """ returns string with T replaced to U"""
    string =  openFileString(file)
    # from list to str and no whitelines
    string = string.strip()
    string = string[:]
    string = string.replace("T","U")
    return string


def writeFile(file):
    """ creates a file with a string, where T's are replaced with U's"""
    string = UreplaceT(file)
    f = open(DirName.joinpath("rna02.txt"), "w+")
    f.write(string)
    f.close()


writeFile(FileName)