"""
Given: Two DNA strings s and t of equal length (not exceeding 1 kbp).

Return: The Hamming distance dH(s,t).
"""
from pathlib import Path
dirPath = Path.cwd().joinpath("coursera").joinpath("rosalind").\
    joinpath("02_bioinformaticsStronghold")
fileName = "hamm01.txt"
filePath = dirPath.joinpath(fileName)


def openFile(filename):
    """ open files with 2 seqs to compare """
    f = open(filename, "r")
    seqs = list()  # list of seqs
    for line in f:
        line = line.rstrip()
        seqs.append(line)
    f.close
    return seqs


def hamming(filename):
    """ returns the hamming distance of seqs from file """
    seqs = openFile(filename)
    # defining lines
    p = seqs[0]
    t = seqs[1]
    hamm = 0
    for i in range(len(p)):
        # comparison
        if p[i] != t[i]:
            hamm += 1
    return hamm


a = hamming(filePath)
print(a)
