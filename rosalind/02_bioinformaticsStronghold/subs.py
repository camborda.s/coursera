"""
Given: two DNA strings s and t (each of length at most 1 kbp)

Return: All locations of t as a substring of s
"""

from pathlib import Path
dirPath = Path.cwd().joinpath("coursera").joinpath("rosalind").\
    joinpath("02_bioinformaticsStronghold")
fileName = "subs01.txt"
filePath = dirPath.joinpath(fileName)


def openFile(filename):
    """ opens file with string s and substring t """
    f = open(filename, "r")
    lines = list()
    for line in f:
        line = line.rstrip()
        lines.append(line)
    f.close
    return lines


def naive(filename):
    """ returns the location of substring s in t, given by file """
    lines = openFile(filename)
    # defining s and t from file
    t = lines[0]
    s = lines[1]
    locations = list()
    # naive algorithm
    for i in range(0, len(t)):
        checkT = t[i:len(s)+i]
        # must be same length
        if len(checkT) != len(s):
            break
        if checkT == s:
            # locations
            locations.append(i+1)
    return locations


a = naive(filePath)
result = ""
for i in a:
    result = " ".join([result, str(i)])
result = result.strip()
print(result)
