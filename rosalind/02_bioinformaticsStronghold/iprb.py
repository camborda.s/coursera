"""
Given: Three positive integers k, m, and n, representing a population
containing k+m+n organisms: k individuals are homozygous dominant for a factor,
m are heterozygous, and n are homozygous recessive.

Return: The probability that two randomly selected mating organisms will
produce an individual possessing a dominant allele (and thus displaying the
dominant phenotype). Assume that any two organisms can mate
"""


def domAllele(k, m, n):
    """ returns possibility of dominant allele in population q, which is the
    sum of k(dominant), m(heterozygous) and n(resessive) """
    # 1 - posibility of recesive
    q = k + m + n
    # res * res
    res1 = n/q * (n-1)/(q-1)
    # res * hete
    res2 = n/q * (0.5*(m/(q-1)))
    # hete * res (order matters)
    res3 = m/q * (0.5*(n/(q-1)))
    # hete * hete (1/4 due to punnet square)
    res4 = m/q * (0.25*(m-1)/(q-1))
    return 1-res1-res2-res3-res4

a = domAllele(16, 30, 15)
print(round(a,5))
