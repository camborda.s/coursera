"""
Given: Six nonnegative integers, each of which does not exceed 20,000. The
integers correspond to the number of couples in a population possessing each
genotype pairing for a given factor. In order, the six given integers represent
the number of couples having the following genotypes:

AA-AA
AA-Aa
AA-aa
Aa-Aa
Aa-aa
aa-aa
Return: The expected number of offspring displaying the dominant phenotype in
the next generation, under the assumption that every couple has exactly two
offspring.
"""


def expectOffspring(a, b, c ,d ,e ,f):
    """ returns the expected number of offspring showing dominant traits, given
    that each number of pairs (a, b, c ,d ,e ,f) have an offspring of 2"""
    # total population - recessive population
    total = 2 * (a + b + c + d + e + f)
    # aa aa
    res1 = 2 * f
    # Aa aa
    res2 = 2 * e * 0.5
    # Aa Aa
    res3 = 2 * d * 0.25
    return total-res1-res2-res3


a =expectOffspring(17126, 19010, 17478, 17437, 17557, 16058)
print(a)