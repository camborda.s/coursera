"""
Given: A DNA string s of length at most 1000 bp.

Return: The reverse complement s^c of s.
"""
from pathlib import Path
DirName = Path.cwd().joinpath("coursera").joinpath("rosalind").\
    joinpath("02_bioinformaticsStronghold")
FileName = "revc01.txt"
FilePath = DirName.joinpath(FileName)


def openFileString(file):
    """ creates a string from file """
    string = ""
    f = open(FilePath, "r")
    for line in f:
        line = line.rstrip()
        newLine = "".join([line])
        string = " ".join([string, newLine])
    f.close()
    return string


def reverseComp(file):
    """ returns from a file with a sequence, its reversed complement"""
    string = openFileString(file)
    string = string.strip()
    dictRev = {"A":"T","T":"A","C":"G","G":"C"}
    stringRev = ""
    for i in range(len(string)):
        reverseNuc = dictRev[string[i]]
        # string has to go behind, otherwise is not reversed
        stringRev = "".join([reverseNuc, stringRev])
    return stringRev

def writeFile(file):
    """ creates a file with the reverse complement for given file"""
    stringRev = reverseComp(file)
    f = open(DirName.joinpath("revc02.txt"), "w+")
    f.write(stringRev)
    f.close()

writeFile(FileName)