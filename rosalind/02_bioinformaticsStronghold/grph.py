"""
Given: A collection of DNA strings in FASTA format having total length at most
10 kbp.

Return: The adjacency list corresponding to O3. You may return edges in any
order.
"""
from pathlib import Path
DirName = Path.cwd().joinpath("coursera").joinpath("rosalind").\
    joinpath("02_bioinformaticsStronghold")
FileName = "grph01.fasta"
FilePath = DirName.joinpath(FileName)


def openFasta(filename):
    """ opens FASTA file and return as dictionary headers with sequences """
    f = open(filename, "r")
    # list of sequences
    seqs = dict()
    for line in f:
        line = line.rstrip()
        # header
        if line[0] == ">":
            name = line.split()
            name = name[0][1:]
            seqs[name] = ""
        # sequence
        else:
            seqs[name] += line
    f.close()
    return seqs


def searchKmer(filename):
    """ compares the 3-mers. Suffix and prefix for each seq. Return pairs
    where there is an overlap"""
    seqs = openFasta(filename)
    seqsList = []
    # transform into list
    for key, value in seqs.items():
        seqsList.append([key, value])
    # create dictionary of prefix
    prefixDict = dict()
    # only prefix into consideration, not other k-mers
    for i in range(len(seqsList)):
        prefix = seqsList[i][1][:3]
        try:
            prefixDict[prefix].add(seqsList[i][1])
        except KeyError:
            prefixDict[prefix] = {seqsList[i][1]}
    # compare and create uniques
    uniqueIncident = set()
    for j in range(len(seqsList)):
        read = seqsList[j][1]
        readName = seqsList[j][0]
        suffix = seqsList[j][1][-3:]
        if suffix in prefixDict.keys():
            values = prefixDict[suffix]
            for x in values:
                if read != x:
                    for i, j in seqsList:
                        # print(i, j)
                        if x == j:
                            key = i
                            break
                    uniqueIncident.update([(readName, key)])
    return uniqueIncident


def formatOutput(filename):
    """ formats the overlaps in a human-friendly output and return it in a new
    file """
    uniqueIncident = searchKmer(filename)
    f = open(DirName.joinpath("grph02.txt"), "w+")
    for i, j in uniqueIncident:
        f.write(" ".join([i, j, "\n"]))
    f.close()


formatOutput(FilePath)
