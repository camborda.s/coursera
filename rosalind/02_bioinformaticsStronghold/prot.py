"""
Given: An RNA string s corresponding to a strand of mRNA (of length at most
1 kbp).

Return: The protein string encoded by s.
"""
from pathlib import Path
dirPath = Path.cwd().joinpath("coursera").joinpath("rosalind").\
    joinpath("02_bioinformaticsStronghold")
fileName = "prot01.txt"
filePath = dirPath.joinpath(fileName)
# defining proteins
protDict = {
    # phenylalanine
    "UUU": "F",\
    "UUC": "F",\
    # Leucine
    "UUA": "L",\
    "UUG": "L",\
    "CUU": "L",\
    "CUC": "L",\
    "CUA": "L",\
    "CUG": "L",\
    # isoleucine
    "AUU": "I",\
    "AUC": "I",\
    "AUA": "I",\
    # Methionine (Start Codon
    "AUG": "M",\
    # valine
    "GUU": "V",\
    "GUC": "V",\
    "GUA": "V",\
    "GUG": "V",\
    # serine
    "UCU": "S",\
    "UCC": "S",\
    "UCA": "S",\
    "UCG": "S",\
    # proline
    "CCU": "P",\
    "CCC": "P",\
    "CCA": "P",\
    "CCG": "P",\
    # threonine
    "ACU": "T",\
    "ACC": "T",\
    "ACA": "T",\
    "ACG": "T",\
    # alanine
    "GCU": "A",\
    "GCC": "A",\
    "GCA": "A",\
    "GCG": "A",\
    # tyrosine
    "UAU": "Y",\
    "UAC": "Y",\
    # stop codons
    "UAA": "",\
    "UAG": "",\
    # histidine
    "CAU": "H",\
    "CAC": "H",\
    # glutamine
    "CAA": "Q",\
    "CAG": "Q",\
    # asperagine
    "AAU": "N",\
    "AAC": "N",\
    # lysine
    "AAA": "K",\
    "AAG": "K",\
    # aspartic acid
    "GAU": "D",\
    "GAC": "D",\
    # glutamic acid
    "GAA": "E",\
    "GAG": "E",\
    # cysteine
    "UGU": "C",\
    "UGC": "C",\
    # stop codon
    "UGA": "",\
    # trytophan
    "UGG": "W",\
    # arginine
    "CGU": "R",\
    "CGC": "R",\
    "CGA": "R",\
    "CGG": "R",\
    # serine
    "AGU": "S",\
    "AGC": "S",\
    # arginine
    "AGA": "R",\
    "AGG": "R",\
    # glycine
    "GGU": "G",\
    "GGC": "G",\
    "GGA": "G",\
    "GGG": "G"}


def fileOpen(filename):
    """ returns the strings """
    f = open(filename, "r")
    for line in f:
        line = line.rstrip()
        seq = line
        f.close
    return seq


def searchProt(filename):
    seq = fileOpen(filename)
    protSeqs = ""
    for i in range(0, len(seq), 3):
        tryout = seq[i:i+3]
        if tryout in protDict:
            protSeqs = "".join([protSeqs, protDict[tryout]])
    print(protSeqs)


searchProt(filePath)
