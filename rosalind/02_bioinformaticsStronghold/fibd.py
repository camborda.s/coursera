"""
Given: Positive integers n≤100 and m≤20.

Return: The total number of pairs of rabbits that will remain after the n-th
month if all rabbits live for m months.
"""
def fuboRabbitLow(n, m=1):
    """ For given months n, returns the amounts of rabbits
    pairs, given than a rabbit lives only m months"""
    # [number of rabbits, age]
    pairs = []
    # baby rabbits (counted in population)
    pairsBaby = 1
    # new born rabbits (not counted in population)
    pairsInter = 0
    for time in range(n):
        # convert baby into mature
        if len(pairs) > 0:
            # add age
            for i in range(len(pairs)):
                pairs[i][1] += 1
            # kill old mature rabbits
            for i in range(len(pairs)):
                try:
                    if pairs[i][1] >= m:
                        pairs.pop(i)
                except:
                    break
            for i in range(len(pairs)):
                pairsInter += pairs[i][0]
        if pairsBaby > 0:
            pairs += [[pairsBaby, 0]]
        # new born to population
        pairsBaby = pairsInter
        pairsInter = 0
    number = 0
    for i in range(len(pairs)):
        number = number + pairs[i][0]
    return number

a = fuboRabbitLow(88,20)
print(a)