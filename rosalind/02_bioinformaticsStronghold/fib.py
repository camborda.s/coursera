"""
Given: Positive integers n≤40 and k≤5.

Return: The total number of rabbit pairs that will be present after n months,
if we begin with 1 pair and in each generation, every pair of reproduction-age
rabbits produces a litter of k rabbit pairs (instead of only 1 pair).
"""
# Fn = Fn-1 + Fn-2
# initial amount of rabbits 2 (one pair)


def FuboRabbits(n, k=1):
    """ For given number of months n, and number of pair baby k, returns the
    number. NAIVE APPROACH. TIME CONSUMING """
    # defaults
    pairs = list()
    interbaby = list()  # newborns
    pairBaby = [1]
    for i in range(n):
        # for each pair of mature rabbit, there should be a new pair of babies
        for x in pairs:
            for j in range(k):
                interbaby.append(x)
        # for each baby, turn them mature
        if len(pairBaby) > 0:
            for baby in pairBaby:
                pairs.append(baby)
        # add newborn to babypopulation
        pairBaby = interbaby
        interbaby = list()
    print(len(pairs))


# FuboRabbits(5)
def fuboRabbitLow(n, k=1):
    """ For given number of months n, and number of pair baby k, returns the
    number."""
    # idea is the same as above but working with intergers and not list
    pairs = 0
    pairsBaby = 1
    pairsInter = 0
    for i in range(1,n+1):
        pairsInter = pairs * k
        if pairsBaby > 0:
            pairs += pairsBaby
        # add newborn to babypopulation
        pairsBaby = pairsInter
        pairsInter = 0
    return pairs

a = fuboRabbitLow(35,3)
print(a)