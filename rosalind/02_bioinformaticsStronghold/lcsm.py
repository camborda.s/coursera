"""
Given: A collection of k (k≤100) DNA strings of length at most 1 kbp each in
FASTA format.

Return: A longest common substring of the collection. (If multiple solutions
exist, you may return any single solution.)
"""
from pathlib import Path
DirName = Path.cwd().joinpath("coursera").joinpath("rosalind").\
    joinpath("02_bioinformaticsStronghold")
FileName = "lcsm01.fasta"
FilePath = DirName.joinpath(FileName)


def openFasta(filename):
    """ opens FASTA file and return as dictionary headers with sequences """
    f = open(filename, "r")
    # list of sequences
    seqs = dict()
    for line in f:
        line = line.rstrip()
        # header
        if line[0] == ">":
            name = line.split()
            name = name[0][1:]
            seqs[name] = ""
        # sequence
        else:
            seqs[name] += line
    f.close()
    return seqs


def shortestSeqKmer(filename, n):
    """ search for the shortest seq and creates a set of n-mers"""
    seqs = openFasta(filename)
    # into list
    seqsList = list()
    for key, value in seqs.items():
        seqsList.append(value)
    # search shortest
    seqsList = sorted(seqsList)
    shortestSeq = seqsList[0]
    for i in range(len(seqsList)):
        seqLen = seqsList[i]
        if len(seqLen) < len(shortestSeq):
            shortestSeq = seqLen
    # create dict
    kmerSet = set()
    for i in range(len(shortestSeq)):
        kmer = shortestSeq[i:i+n]
        if len(kmer) == n:
            kmerSet.update([kmer])

    return seqsList, kmerSet, shortestSeq


def compareKmer(seqsList, kmerSet):
    """ compares the set of k-mers with the list of sequences"""
    # set to avoid duplicates
    setCandidate = set()
    for kmer in kmerSet:
        # verify in kmerList
        for seq in seqsList:
            if seq.find(kmer) != -1:
                # add to list of candidates
                setCandidate.update([kmer])
    setCandidate = sorted(setCandidate)
    return setCandidate


def verificationLeft(string, seqsList):
    """ search string in given List and adds more strings to the search looking
    on the left """
    for i in range(len(seqsList)):
        if seqsList[i].find(string) == 0:
            # if zero, take next value
            break
        # defining new candidate
        candidate = seqsList[i][seqsList[i].find(
            string)-1:seqsList[i].find(string)+len(string)]
        # check if candidate is not in List
        for j in seqsList:
            if j.find(candidate) == -1:
                # There is no more
                return string
        # search again
        something = verificationLeft(candidate, seqsList)
        # if border
        if something is None:
            return candidate
        else:
            return something


def verificationRight(string, seqsList):
    """ search string in given List and adds more strings to the search looking
    on the right """
    if string is None:
        return
    for i in range(len(seqsList)):
        if seqsList[i].find(string) > len(seqsList[i]) + 1 - len(string):
            # if zero, take next value
            break
        # defining new candidate
        candidate = seqsList[i][seqsList[i].find(
            string):seqsList[i].find(string)+len(string)+1]
        # check if candidate is not in List
        for j in seqsList:
            if candidate == string:
                return string
            if j.find(candidate) == -1:
                # There is no more
                return string
        # search again
        something = verificationRight(candidate, seqsList)
        # if border
        if something is None:
            return candidate
        else:
            return something


def verification(candidate, seqsList):
    """ verifies if kmer can be longer """
    # first to the left
    new = verificationLeft(candidate, seqsList)
    # then to the right
    b = verificationRight(new, seqsList)
    return b


def returnSeq(filename, n):
    """ search in list of seqs, if the kmer is there, if not search for a
    (k-1)-mer. Otherwise, verify """
    seqsList, kmerSet, shortestSeq = shortestSeqKmer(filename, n)
    setCandidate = compareKmer(seqsList, kmerSet)
    finalList = list()
    for kmer in setCandidate:
        new = verification(kmer, seqsList)
        if new is not None:
            finalList.append(new)
    # if any None
    longest = (max(finalList, key=len))
    return longest


print(returnSeq(FilePath, 3))
print(returnSeq(FilePath, 4))
print(returnSeq(FilePath, 5))
