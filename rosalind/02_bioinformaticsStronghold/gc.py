"""
Given: At most 10 DNA strings in FASTA format (of length at most 1 kbp each).

Return: The ID of the string having the highest GC-content, followed by the
GC-content of that string.
"""
from pathlib import Path
dirPath = Path.cwd().joinpath("coursera").joinpath("rosalind").\
    joinpath("02_bioinformaticsStronghold")
fileName = "gc01.txt"
filePath = dirPath.joinpath(fileName)


def openFasta(filename):
    """ opens FASTA file and return as dictionary headers with sequences """
    f = open(filename, "r")
    # list of sequences
    seqs = dict()
    for line in f:
        line = line.rstrip()
        # header
        if line[0] == ">":
            name = line.split()
            name = name[0][1:]
            seqs[name] = ""
        # sequence
        else:
            seqs[name] += line
    f.close()
    return seqs


def countCG(filename):
    """ counts the CG's in each sequences and returns dictionary with its
    corresponding porcentages """
    seqs = openFasta(filename)
    dictCG = dict()
    for key, value in seqs.items():
        countCG = 0
        lengthSeq = len(value)
        # checks for n CG's
        for i in range(lengthSeq):
            if value[i] == "C" or value[i] == "G":
                countCG += 1
        # transforming into percetanges
        dictCG[key] = round(100*(countCG/lengthSeq), 6)
    return dictCG


def checkHighest(filename):
    """ returns id with highest CG porcentage """
    dictCG = countCG(filename)
    highestName = ""
    highestVal = 0
    for key, value in dictCG.items():
        if value > highestVal:
            highestVal = value
            highestName = key
    print(highestName)
    print(highestVal)


checkHighest(filePath)
