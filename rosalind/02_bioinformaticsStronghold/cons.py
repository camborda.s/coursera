"""
Given: A collection of at most 10 DNA strings of equal length (at most 1 kbp)
in FASTA format.

Return: A consensus string and profile matrix for the collection. (If several
possible consensus strings exist, then you may return any one of them.)
"""
from pathlib import Path
import random
dirPath = Path.cwd().joinpath("coursera").joinpath("rosalind").\
    joinpath("02_bioinformaticsStronghold")
fileName = "cons01.fasta"
filePath = dirPath.joinpath(fileName)


def openFasta(filename):
    """ opens FASTA file and return as dictionary headers with sequences """
    f = open(filename, "r")
    # list of sequences
    seqs = dict()
    for line in f:
        line = line.rstrip()
        # header
        if line[0] == ">":
            name = line.split()
            name = name[0][1:]
            seqs[name] = ""
        # sequence
        else:
            seqs[name] += line
    f.close()
    return seqs


def createMatrix(filename):
    """ creates matrix for dna strings and profile according to given file """
    seqs = openFasta(filename)
    # dna string matrix
    # n is equal for all seqs
    n = len(list(seqs.values())[0])
    dnaMat = list()
    for i in range(len(seqs.values())):
        dnaMat.append([0] * n)
    # profile matrix
    profileMat = list()
    for i in range(4):
        profileMat.append([0]*n)
    return dnaMat, profileMat, seqs


def fillMatrix(filename):
    """ fills dnaMat and profileMat with its respective values from given
    file and returns profileMat """
    dnaMat, profileMat, seqs = createMatrix(filename)
    # seqs into list
    seqsList = list()
    for key, value in seqs.items():
        seqsList.append((key,value))
    # filling dnaMat
    for i in range(len(dnaMat)):
        for j in range(len(dnaMat[i])):
            # 1 because of tuple
            dnaMat[i][j] = seqsList[i][1][j]
    nucleotideList = ["A", "C", "G", "T"]
    # filling profileMat
    for i in range(len(profileMat)):
        for h in range(len(dnaMat[0])):
            amountNuc = 0
            for j in range(len(dnaMat)):
                if nucleotideList[i] == dnaMat[j][h]:
                    amountNuc += 1
            profileMat[i][h] = amountNuc
    return profileMat, nucleotideList


def consensusString(filename):
    """ returns the first concensusString with nucleotideList and profile
    Matrix """
    profileMat, nucleotideList = fillMatrix(filename)
    string = ""
    for j in range(len(profileMat[0])):
        highestVal = 0
        highestNuc = ""
        for i in range(len(nucleotideList)):
            if profileMat[i][j] > highestVal:
                # replace
                highestVal = profileMat[i][j]
                highestNuc = nucleotideList[i]
        string = "".join([string, highestNuc])
    return string, profileMat, nucleotideList


def formatStrMatrix(filename):
    """ prints the in specific format the answer for rosalind:
    concensus string, following with the profile matrix, where the first
    column shows the corresponding nucleotide """
    string, profileMat, nucleotideList = consensusString(filename)
    f = open(dirPath.joinpath("cons02.fasta"), "w+")
    row = ""
    f.write("".join([string,"\n"]))
    for i in range(4):
        temp = str(profileMat[i])
        temp = temp.replace("[", "").replace("]", "").replace(",", "")
        temp = "".join([nucleotideList[i], ": ", temp,"\n"])
        f.write(temp)
    f.close()
    

formatStrMatrix(filePath)