from pathlib import Path
cwd = Path.cwd()
dirpath = Path.cwd().joinpath("coursera")
pythonpath = dirpath.joinpath("python")
text1 = str(pythonpath.joinpath("week4_myfile"))
text2 = str(pythonpath.joinpath("week4_myfile2"))
# Reading a file
f2=open(str(pythonpath.joinpath("week4_myfile2")),"r")
# Writing a file
f2=open(text2,"w")
# print(file)
f2.write("This is a NEW line")
f2.close()
print("--------ReadingFileTest------------")
try:
    f1 = open(text1)
except IOError:
    print("The file myfile does not exist!!")
#first reading form
f1.seek(0)
f1.read()
print("________")
f1.seek(0)
f1.readline()
# Writing into a file
f1=open(text1,"a")
f1.write("This is a four line")
f1.close()
print("Exercise: Build a dictionary containing all sequences from a FASTA file")
# Pseudo code: Open file->Check for headers->get sequence name and create new entry in dictionary/ IF NOT -> update sequence in dictionary/ Check for more lines /IF NOT close file
#Defining path
fasta=str(pythonpath.joinpath("week4.fa"))
#Reading and opening file
try:
    f=open(fasta)
    print("File found")
except IOError:
    print("File not found")
seqs={}
for line in f:
    # discard the newline at the end (if any)
    line=line.rstrip()
    #Differentiate headers from seqs
    if line[0]==">":
        words=line.split()
        name=words[0][1:]
        seqs[name]=""
    else: #if sequence
        seqs[name]=seqs[name]+line
f.close()
# print(seqs) #to check 
print("----Retriving Data from dictionaries----")
for name,seq in seqs.items():
    pass
filename ="myfle.tar.gz"
filename2 ="myfile"
def get_ex1(filename):
    return(filename.split(".")[-1])
def get_ex2(filename):
    import os.path
    return(os.path.splitext(filename)[1])
def get_ex3(filename):
    return filename[filename.rfind("."):][1:]

get_ex3(filename)
get_ex3(filename2)