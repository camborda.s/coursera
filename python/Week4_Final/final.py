from pathlib import Path
DirName = Path.cwd().joinpath("coursera").joinpath("python").joinpath("Week4_Final")
FileName = str(DirName.joinpath("dna.example.fasta"))
FileName2 = str(DirName.joinpath("dna.example2.fasta"))
FileName3 = str(DirName.joinpath("dn2.fasta"))

class Sequence:
    def CreateDic(filename,repetido=0):
        """"Creates list with id, name and length for different functions of given file"""
        # Read the file
        f = open(filename)
        if repetido == 0:
            print("Working with file:",str(filename))
            print("-------------------------------------")
        SeqsNames = {}
        SeqsLen = {}
        for line in f:
            #first discard new line at end
            line=line.rstrip()
            #Differenciate seqs from line
            if line[0]==">":
                # print("Header Found")
                words=line.rsplit()
                name=words[0][1:]
                #build dict
                SeqsNames[name]=""
                #SeqsLen is for counting the seqs
                SeqsLen[name]=""
            else:
                SeqsNames[name]=SeqsNames[name]+line
                SeqsLen[name]=len(SeqsNames[name])
        f.close()
        return SeqsNames, SeqsLen

    def CreateCompSeq(filename):
        """Creates a list of ID with is complementary sequence for given file"""
        #defining complements and bringing seqs from CreateDicion
        baseComplement = {"A":"T","T":"A","C":"G","G":"C"}
        SeqsNames, SeqsLen=Sequence.CreateDic(filename)
        CompSeq={}
        for k,v in SeqsNames.items():
            ListSeq= list(v)
            ListSeq= [baseComplement[base] for base in ListSeq]
            CompStr = "".join(ListSeq)
            CompSeq[k]=CompStr
        complement = 1
        return CompSeq, SeqsLen, complement
    # (1) How many records are in the file?
    def PrintSeq(filename,complement,repetido=0):
        """Prints the id and length of the longest and shortest IDs in given file. Works with complementary sequences"""
        if complement == 0:
            if repetido == 0:
                print("Reading 5' to 3'")
            SeqsNames, SeqsLen =Sequence.CreateDic(filename,repetido=repetido)
        else:
            if repetido == 0:
                print("Complement!")
                print("Reading 3' to 5'")
            SeqsNames, SeqsLen,complement= Sequence.CreateCompSeq(filename)
        #Create a list of sorted values
        SeqsLenSorted = sorted(SeqsLen.items(), key=lambda x: x[1])
        #count the dict
        if repetido == 0:
            print("Number of Seqs in the file: %d" %len(SeqsNames))
        #(2) What are the lengths of the sequences in the file? What is the longest sequence and what is the shortest sequence?
        #Longest and shortest sequences
            print("Longest Sequence: ", (SeqsLenSorted[-1][0]), "\nwith", SeqsLenSorted[-1][1],"BP")
            print("Shortest Sequence: ", (SeqsLenSorted[0][0]), "\nwith", SeqsLenSorted[0][1],"BP")
            # print("-------------------------------------")
            print("_____________________________________")
        return SeqsLenSorted,SeqsNames,complement

    # (3.1) what is the length of the longest ORF in the file? What is the identifier of the sequence containing the longest ORF?
    def OEF(filename,frame=0,complement=0,repetido=0):
        """Searchs in given file for ORF's and creates list of the ORF and its length. Starting position is optional. Works with complementary sequences."""
        SeqsLenSorted,SeqsNames,complement =Sequence.PrintSeq(filename,complement=complement,repetido=repetido)
        #Define StopCodon , Startcodon and defaults
        if repetido == 0:
            print("Searching for ORF's")
        print("Starting position is:",frame)
        StartCodon = "ATG"
        StopCodonList=["TGA","TAG","TAA"]
        ListPosition=list()
        ListStart=list()
        PoStart=0
        PoStop=0
        StartFound=0
        StopFound=0
        LengthORF=0
        SeqsNames=list(SeqsNames.items())
        #creates a string of each sequence is the list SeqsID
        for i in range(len(SeqsNames)):
            id=SeqsNames[i][0]
            # code2 is the triplet which is going to be read
            code2=list()
            # print("Looking in new id:",i)
            code = SeqsNames[i][1]
            #Looking for Startcodons - step of 3 because of triplets.
            for t in range(frame,len(code),3):
                code2+=code[t:t+3]
                # print(code2)
                if StartCodon in code2:
                    # print("Hay uno")
                    PoStart=t
                    # print("position:",t)
                    code2=""
                    StartFound = 1
                    break
                else:
                    pass
                    # print("No hay")
                code2=""
            #Looking for stop codons
            # print("Searching for StopCodons")
            for t_2 in range(0,(len(code)),3):
                code2+=code[t_2:t_2+3]
                # print(code2)
                if code2 in StopCodonList:
                    # print("Hay Stop")
                    # print("position:",t_2)
                    code2=""
                    PoStop=t_2
                    StopFound = 1
                    break
                    # continue
                else:
                    # print("No hay STOP")
                    pass
                code2=""
            # Bulding the dictionary - Defining the content (Positions should give total length)
            if (StopFound and StartFound == 1) and ((PoStart-PoStop)<0):
                LengthORF=PoStop-PoStart+2
                ListPosition.append((id,LengthORF,PoStart))
            else:
                LengthORF=0
                ListPosition.append((id,LengthORF,0))
            # print(ListPosition)
            PoStart=0
            PoStop=0
            StopFound=0
            StartFound=0
            LengthORF=0
        # Filtering the dict
        SeqORF=dict()
        for key, value, pos in ListPosition:
            # print(key)
            NoORF=0
            ORFfound=1
            if value == NoORF:
                # print("FALSE")
                ORFfound=1
                continue
            else:
                ORFfound=0
            if ORFfound == 0:
                # print("This is good")
                # print(value)
                SeqORF[key] = (value,pos)
        #Building sorted list of ORFs and printing number of ORFS the length of longest ORF
        # for x in SeqORFSorted:

        SeqORFSorted=sorted(SeqORF.items(), key=lambda x: x[1])
        print("Total number of ORF's:",len(SeqORFSorted))
        print("Longest ORF: ", (SeqORFSorted[-1][0]), "\nwith", SeqORFSorted[-1][1],"BP")
        print("Position of longest ORF:")
        print("-------------------------------------")
        return SeqORFSorted
    #(3.2) For a given sequence identifier, what is the longest ORF contained in the sequence represented by that identifier? What is the starting position of the longest ORF in the sequence that contains it
    def CheckMultiFrame(filename,id,complement=0,frame=0,multiple=0):
        """Check if user sets multiple frame option. Works with complementary sequences"""
        if multiple == 0:
            Sequence.CheckORF(filename,id=id,complement=complement,frame=frame)
        else:
            print("Checking multiple starting frames")
            # Frame 0
            Sequence.CheckORF(filename,id=id,complement=complement,frame=0,repetido=0)
            for frame in range(1,3):
                Sequence.CheckORF(filename,id=id,complement=complement,frame=frame,repetido=1)
        return

    def CheckORF(filename,id,complement=0,frame=0,repetido=0):
        """Checks if given ID has an ORF in given file and returns the length of the respective ORF. Setting a frame position is optional. Works with complementary sequences"""
        SeqORFSorted = Sequence.OEF(filename,complement=complement,frame=frame,repetido=repetido)
        SeqORFSorted= dict(SeqORFSorted)
        if id in SeqORFSorted:
        # if id in newDic2:
            print("ORF length of '",id,"' is:",SeqORFSorted[id])
        else:
            print("Given ID was not found.")
        print("-------------------------------------")
        return
# Given a length n, your program should be able to identify all repeats of length n in all sequences in the FASTA file. Your program should also determine how many times each repeat occurs in the file, and which is the most frequent repeat of a given length.
    def CheckRep(filename,n):
        """For a given length n in given file, checks for repeats within the file and returns the most common repeat with its respective number of repetitions.
        """
        # Repeat must be at leasr 2
        if n <=1:
            print("Not possible. n should be larger than 1")
            return
        #Defining defaults and calling file from CreateDic
        SeqsNames, SeqsLen = Sequence.CreateDic(filename)
        SeqFile = ""
        Repeat = ""
        RepSeq=list()
        for key, value in SeqsNames.items():
            SeqFile+=str.join("",value)
        # print(len(SeqFile))
        for i in range(len(SeqFile)):
            # Defining Repeat
            Repeat = SeqFile[i:i+n]
            #N has to remain the same length
            if len(Repeat)<n:
                pass
            else:
                if SeqFile.count(Repeat) > 1:
                    RepSeq.append((Repeat,SeqFile.count(Repeat)))
        print("Number of possible repeats with length",n,":",len(RepSeq))
        RepSeq = sorted(RepSeq, key=lambda tup:tup[1] )
        #RepSeq needs at least one repetition
        if len(RepSeq) == 0:
            print("No repeats found")
            return
        else:
            print("The most common repeat is",tuple(RepSeq[-1])[0],"with",tuple(RepSeq[-1])[1],"repetitions within the file.")
        return
