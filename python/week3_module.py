"""
Modules created to build functions utilized in week3.py
"""
def cg_por (dna):
    """Computes the CG percentage of a DNA sequence"""
    dna=dna.upper()
    print(dna)
    cg= dna.count("A")+dna.count("C")
    dnaLen= len(dna)
    if dnaLen == 0:
        return("Please try again")
    else:
        output= cg*100/dnaLen
        return(output)
# help(cg_por)
dna="gctagtcgtcgt"
print("---------------")
def stopCodon(dna,frame=0,comp=0):
    """Check if given DNA sequence have in-frame stop codons.
    frame=starting position. By default it also gives the complementary strand of DNA"""
    dna=dna.upper()
    StopCodonList=["TGA","TAG","TAA"]
    def analysis (dna,frame):
        print(dna)
        for i in range(frame,len(dna),3):
            codon = dna[i:i+3]
            print(codon)
            if codon in StopCodonList:
                print("Codon Found")
                break
            else:
                print("No Stop-Codon found")
    def complement(dna):
        """Return complementary sequence string"""
        baseComplement = {"A":"T","T":"A","C":"G","G":"C"}
        listSeq= list(dna)
        listSeq= [baseComplement[base] for base in listSeq]
        print("".join(listSeq))
    print("Checking DNA sequence")
    analysis(dna,frame)
    if comp==1:
        complement(dna)
    print("___________________")
    print("Checking for reversed sequence")
    dna=dna[::-1]
    analysis(dna,frame)
    if comp==1:
        complement(dna)
