import sys
from week3_module import *
print("__version__")

stopCodon(dna,2,1)
print("-------------------------")
def swap1(x,y) :
    x=y
    y=x
    return(x,y)

def swap2(x,y) :
    return(y,x)

def swap3(x,y) :
    z=x
    x=y
    y=z
    return(x,y)

def swap4(x,y) :
    x,y=y,x
    return(x,y)
print("-------------------------")
def f1(x):
    if (x > 0):
        x = 3*x
        x = x / 2
    return x
def d2(x):
    if (x > 0):
        x = 3*x
    x = x / 2
    return x
f1(-1.6)
d2(-1.6)
print("-------------------------")
# def function2(length):
#     if length > 0:
#         print(length)
#         function2(length - 1)
def compute(n,x,y) :
    if n==0 : return x
    return compute(n-1,x+y,y)
# compute(-2,2,3)
# compute(2,10,4)
# compute(2,4,5)

def valid_dna1(dna):
    for c in dna:
        if c in 'acgtACGT':
            return True
        else:
            return False
# valid_dna1("hhhhg")
# valid_dna1("cgt")
def valid_dna2(dna):
    for c in dna:
        if 'c' in 'acgtACGT':
            return 'True'
        else:
            return 'False'
# valid_dna2("hhhhg") #NEIN
def valid_dna4(dna):
    for c in dna:
        if not c in 'acgtACGT':
            return False
    return True
# valid_dna4("Hhhhgh")
# valid_dna4("acgt")
def valid_dna3(dna):
    for c in dna:
        flag = c in 'acgtACGT'
    return flag
# valid_dna3("hhghh")
# valid_dna3("cgt")
print("--------------------")
