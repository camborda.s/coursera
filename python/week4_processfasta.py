
def usage():
    print("""
processfasta.py builds a dictionary with all sequences from a FASTA file and builds a dictionary withh all sequence bigger than a given length

processfasta.py [-h] [-l <length>] <filename>
    -h              print this message
    -l <length>     filter all sequences with a length
                    smaller than <length>
                    (default 0)
    <filename>      the file has to be in FASTA format      
""")
import sys
import getopt
from pathlib import Path

o, a =getopt.getopt(sys.argv[1:],"l:h") #: means obligatory -> l is obligatory
opts = {}
seqlen=0;
for k,v in o:
    opts[k] =v
if "-h" in opts.keys():
    usage(); sys.exit()
if len(a) < 1:
    usage(); sys.exit("Input fasta file is missing")
if "-l" in opts.keys():
    if int(opts["l"])<0:
        print("Length of sequence should be positive!"); sys.exit(0)
    seqlen=opts["-l"]

cwd=Path.cwd()
dirname=Path.cwd().joinpath("coursera").joinpath("python")
# filename=sys.argv[1]
try:
    f=open(filename)
except IOError:
    print("File %s not found" %filename)