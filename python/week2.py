# dna=input("Enter DNA sequence:\n")
print("TEST condition OR")
dna="cccgtgttggt"
if "n" or "N" in dna:
    nbases=dna.count("n")+dna.count("N")
    print("DNA sequence has %d undefined bases" % nbases)
else:
    print("DNA sequence has no undefines bases") 
print("-------------------")
print("TEST while loop")
pos=dna.find("gt",0)
while pos>-1:
    print("Donor splice site candidate at position %d" %pos)
    pos=dna.find("gt",pos+1) #Otherwise, stays in same position
print("-------------------")
print("TEST statements for")
motifs = ["a","aa","aaa"]
for deggi in motifs:
    print(deggi)
print("-------------------")
print("TEST statements range - continue")
nucleotide = "ACGTAHHCCGTAGS"
valid_nucleotide ="ACGT"
correct_nucleotide =""
print("Sequence %s" % nucleotide)
pos_nucleotide = len(nucleotide)
print("len=%d" %pos_nucleotide)
for i in range(pos_nucleotide):
    while nucleotide[i] not in valid_nucleotide :
        print("Invalid nucleotide in %d" % pos_nucleotide)
        pos_nucleotide=pos_nucleotide+1
        print(nucleotide[i])
        break
    if nucleotide[i] not in valid_nucleotide:
        continue #continue to the next [i] of the range
    correct_nucleotide = correct_nucleotide+nucleotide[i]
print("Correct sequence: %s"%correct_nucleotide)
print("--------------------------")
L3 = []                         # line 1
L1 = [1,2,3,4,3]
L2 = [3,4,5,6]
for elem in L1:            # line 2
    if elem in L2 and not elem in L3:            # line 3
        L3.append(elem)    # line 4
print(L3)
print("-------------------------")
fold = 2
if fold > 2 : print("1condition A’")
elif fold>100: print("1condition B")
if fold> 2 or fold<2 : print('condition A')
else : print("’condition B’")
print("--------------------")
mylist=[1,2,2,3,4,5]
d = {}
result = False
for x in mylist:
    if x in d:
        result=True
        break
    d[x] = True
print(d)
print("--------------")
d = {}
mylist=[1,2,2,3,4,5]
result = False
for x in mylist:
    if not x in d:
        d[x]=True
        continue
    result = True
print(d)
print("------------------")
seq="123456"
for i in range(len(seq)) :     # line 1
    # for j in range(i) :
    for j in range(i+1) :        # line 2
        print(seq[j:i])
print("----------------")
i=0 
while i<len(seq) :
      j=0 
      while(j<i) :
                print(seq[j:i])
                j+=1
      i+=1
print("----------------------")
for i in range(1,-23,-3):
    print(i)
print("-------------------------")
i = 1
while i < 100:
          if i%2 == 0 : break
          i += 1
else:
     i=1000
print(i)
x= 10*10**6
print(x)
if x>10 or x<-10: print('big')
elif x>1000000: print('very big')
elif x<-1000000: print('very big')
else : print('small')