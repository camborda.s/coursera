"""First, implement a version of the naive exact matching algorithm that is strand-aware. That is, instead of looking only for occurrences of P in T, additionally look for occurrences of the reverse complement of P in T. If P is ACT, your function should find occurrences of both ACT and its reverse complement AGT in T.

If P and its reverse complement are identical (e.g. AACGTT), then a given match offset should be reported only once. So if your new function is called naive_with_rc, then the old naivefunction and your new naive_with_rc function should return the same results when P equals its reverse complement."""
from pathlib import Path
DirName = Path.cwd().joinpath("coursera").joinpath("python").joinpath("AlgorithmsDNA")
class homework:
    def readgenome(filename):
        """Reads filename and return a string with the sequence from the file"""
        genome =""
        with open(filename ,"r") as f:
            for line in f:
                if not line[0] == ">":
                    #If line then and the like to 
                    genome += line.rstrip()
        return genome

    def readfastq(filename):
        sequences = []
        qualities = []
        with open(filename) as fh:
            while True:
                fh.readline()
                seq = fh.readline().rstrip()
                fh.readline()
                qual = fh.readline().rstrip()
                if len(seq) == 0:
                    break
                sequences.append(seq)
                qualities.append(qual)
        return sequences, qualities
        
    def reverseComplement(s):
        """Return the reverse complement of a string"""
        complement = {"A":"T","T":"A","C":"G","G":"C","N":"N"}
        t=""
        for base in s:
            t = complement[base] + t
        return t
    
    def naive(p,t):
        """Aligns the string p in reference string t and returns its positions if available"""
        occurrence = []
        for i in range(len(t)-len(p)+1):
            match = True
            for j in range(len(p)):
                if not t[i+j] == p[j]:
                    match= False
                    break
            if match:
                occurrence.append(i)
        return occurrence

    def naive_with_rc(p,t):
        print("Regular 5' to 3'")
        occurrence = homework.naive(p=p,t=t)
        print(p)#,occurrence)
        #Checking if occurrences are the same
        print("Checking reverse complement...")
        reverComp = homework.reverseComplement(s=p)
        occurrence_rc = homework.naive(p=reverComp,t=t)
        if p == reverComp:
            print("Reverse complement and the read are the same")
            print("Total length:", len(occurrence))
            return occurrence
        else:
            bothOccu = sorted(occurrence+occurrence_rc)
            print(reverComp)#,occurrence_rc)
            print("Alignment possible at following position:",bothOccu)
            print("Total length:", len(bothOccu))
            return bothOccu

    def naive_2mm(p,t):
        """Aligns the string p in reference string t with a tolerance of maximal 2 missmatches and returns its positions if available"""
        occurrence = []
        for i in range(len(t)-len(p)+1):
            #j is index of p | i+j is sequence
            matchCount = 0
            for j in range(len(p)):
                if t[j+i] ==p[j]:
                    # print(t[i+j],p[j],"Match")
                    matchCount +=1
                else:
                    # print("No match")
                    pass
            if matchCount >=(len(p)-2):
                # print(matchCount,"Solo tiene maximo 2 errores")
                occurrence.append(i)
        return occurrence

    def phredd33toQ(qual):
        return ord(qual)-33

p="ACTTTA"
# t="ACTTACTTGATAAAGT"
t="ACTTTA ACTTTC ACTTCC ACTCCC ACCCCC"
print(homework.naive_2mm(p,t))

# lambda_genome = homework.readgenome(DirName.joinpath("lambda_virus.fa"))
# occurences = homework.naive_2mm("AGGAGGTT",lambda_genome)
# print(occurences)
# print("Length:",len(occurences))


# phix_genome = homework.readgenome(DirName.joinpath('phix.fa'))
# occurrences = homework.naive_2mm('GATTACA', phix_genome)
# print('offset of leftmost occurrence: %d' % min(occurrences))
# print('# occurrences: %d' % len(occurrences))


# occurence = homework.naive_with_rc("AGTCGA",lambda_genome)
# print(min(occurence))
# print(min(homework.naive_with_rc("AGTCGA",lambda_genome)))
"""
For Questions 5 and 6, make a new version of the \verb|naive|naive function called \verb|naive_2mm|naive_2mm that allows up to 2 mismatches per occurrence. Unlike for the previous questions, do not consider the reverse complement here. We're looking for approximate matches for P itself, not its reverse complement.

For example, \verb|ACTTTA| occurs twice in \verb|ACTTACTTGATAAAGT|, once at offset 0 with 2 mismatches, and once at offset 4 with 1 mismatch. So \verb|naive_2mm('ACTTTA', 'ACTTACTTGATAAAGT') should return the list \verb|[0, 4]|.

Hint: See this notebook for a few examples you can use to test your \verb|naive_2mm|naive_2mm function.
"""
# print(len(homework.naive_2mm("TTCAAGCC",lambda_genome)))
# print(min(sorted(homework.naive_2mm("TTCAAGCC",lambda_genome))))
"""
This dataset has something wrong with it; one of the sequencing cycles is poor quality.
Report which sequencing cycle has the problem. Remember that a sequencing cycle corresponds to a particular offset in all the reads. For example, if the leftmost read position seems to have a problem consistently across reads, report 0. If the fourth position from the left has the problem, report 3. Do whatever analysis you think is needed to identify the bad cycle. It might help to review the "Analyzing reads by position" video."""
sequences, qualities = homework.readfastq(DirName.joinpath("ERR037900_1.first1000.fastq"))
qualities_phred33 = list()

for i in range(len(qualities)):
    # print(qualities[i])
    quality_list=list()
    for x in range(len(qualities[i])):
        # print(qualities[i][x])
        quality_number = homework.phredd33toQ(qualities[i][x])
        # print(quality_number)
        quality_list.append(quality_number)
    qualities_phred33.append(quality_list)
#Creating averages for plot
list_average=[]
listOfX=[]
for i in range(len(qualities_phred33[i])):
    listOfX=[]
    for x in range(len(qualities_phred33)):
        listOfX.append(qualities_phred33[x][i])
    average= sum(listOfX)/len(qualities_phred33)
    list_average.append(average)
index_min = min(range(len(list_average)), key=list_average.__getitem__)
print(index_min)

import numpy as np
index_min = np.argmin(list_average)
print(index_min)
