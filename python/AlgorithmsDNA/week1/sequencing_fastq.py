from pathlib import Path
DirName = Path.cwd().joinpath("coursera").joinpath("python").joinpath("AlgorithmsDNA")
FileName = DirName.joinpath("SRR835775_1.first1000.fastq")
def readfastq(filename):
    sequences = []
    qualities = []
    with open(filename) as fh:
        while True:
            fh.readline()
            seq = fh.readline().rstrip()
            fh.readline()
            qual = fh.readline().rstrip()
            if len(seq) == 0:
                break
            sequences.append(seq)
            qualities.append(qual)
    return sequences, qualities
seqs, quals = readfastq(FileName)
# print(seqs[:5],quals[:5])
def phredd33toQ(qual):
    return ord(qual)-33
# print(phredd33toQ("j"))
def createHist(qualities):
    hist = [0] * 50
    for qual in qualities:
        for phred in qual:
            q = phredd33toQ(phred)
            hist[q] +=1
    return hist
h = createHist(quals)
# Matplotlib inline
import matplotlib.pyplot as plt
plot01 = plt.bar(range(len(h)),h)
plt.show(plot01)
########################################
def findCGbyPos(reads):
    #number of gc bases at each position of the reads
    gc = [0]*100
    #total number of bases at each position
    totals = [0] *100
    for read in reads:
        for i in range(len(read)):
            if read[i] == "C" or read[i] == "G":
                gc[i] += 1
            totals[i] += 1
    for i in range(len(gc)):
        if totals[i] > 0:
            gc[i] /= float(totals[i])
    return gc
gc =findCGbyPos(seqs)
plot02 = plt.plot(range(len(gc)),gc)
plt.show(plot02)
#Average GC content is greater than 0.5 -> makes sense as the reads comes from the human genome
import collections
count = collections.Counter()
for seq in seqs:
    count.update(seq)
print(count)