from pathlib import Path
DirName = Path.cwd().joinpath("coursera").joinpath("python").joinpath("AlgorithmsDNA")
def readgenome(filename):
    """Reads filename and return a string with the sequence from the file"""
    genome =""
    with open(filename ,"r") as f:
        for line in f:
            if not line[0] == ">":
                #If line then and the like to 
                genome += line.rstrip()
    return genome
genome = readgenome(DirName.joinpath("phix.fa"))
def naive(p,t):
    """Aligns the string p in reference string t and returns its positions if available"""
    occurrence = []
    for i in range(len(t)-len(p)+1):
        match = True
        for j in range(len(p)):
            if not t[i+j] == p[j]:
                match= False
                break
        if match:
            occurrence.append(i)
    return occurrence
#############################################
def readfastq(filename):
    sequences = []
    qualities = []
    with open(filename) as fh:
        while True:
            fh.readline()
            seq = fh.readline().rstrip()
            fh.readline()
            qual = fh.readline().rstrip()
            if len(seq) == 0:
                break
            sequences.append(seq)
            qualities.append(qual)
    return sequences, qualities
phix_reads, _ = readfastq(DirName.joinpath("ERR266411_1.first1000.fastq"))
numMatched = 0
n= 0
for r in phix_reads:
    r = r[:30]
    matches =naive(r,genome)
    n += 1
    if len(matches) >0:
        numMatched+=1
print(numMatched,"/",n,"reads matched the genome")
"""7 /1000, values very low. probably due to sequencing
errors. (When using the whole reads)
if we change it to e.g r=30 (prefix), then it returns 459
matches. The function is only lookin for single strand.
"""
def reverseComplement(s):
    """Return the reverse complement of a string"""
    complement = {"A":"T","T":"A","C":"G","G":"C","N":"N"}
    t=""
    for base in s:
        t = complement[base] + t
    return t

numMatched = 0
n= 0
for r in phix_reads:
    r = r[:30]
    matches =naive(r,genome)
    #adds reverscomplements
    matches.extend(naive(reverseComplement(r),genome))
    n += 1
    if len(matches) >0:
        numMatched+=1
print(numMatched,"/",n,"reads matched the genome")
"""932 /1000, Now we used the prefix and the reverse complement of the reads giving us a higher value"""