from pathlib import Path
DirName = Path.cwd().joinpath("coursera").joinpath("python").joinpath("AlgorithmsDNA")
def readgenome(filename):
    genome =""
    with open(filename ,"r") as f:
        for line in f:
            if not line[0] == ">":
                #If line then and the like to 
                genome += line.rstrip()
    return genome
genome = readgenome(DirName.joinpath("lambda_virus.fa"))
print(len(genome))

base= {"A":"T","C":"G","T":"A","G":"C",}
counts={"A":0,"T":0,"C":0,"G":0,}
for base in genome:
    counts[base] += 1
print(counts)
#or python module
import collections
print(collections.Counter(genome))