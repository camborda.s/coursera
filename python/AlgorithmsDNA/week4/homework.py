import itertools
from pathlib import Path
DirName = Path.cwd().joinpath("coursera").joinpath("python").joinpath("AlgorithmsDNA").joinpath("week4")
FileName = DirName.joinpath("ads1_week4_reads.fq")
def readfastq(filename):
    sequences = []
    qualities = []
    with open(filename) as fh:
        while True:
            fh.readline()
            seq = fh.readline().rstrip()
            fh.readline()
            qual = fh.readline().rstrip()
            if len(seq) == 0:
                break
            sequences.append(seq)
            qualities.append(qual)
    return sequences, qualities
seqs, quals = readfastq(FileName)

def overlap(a, b, min_length=3):
    """ Return length of longest suffix of 'a' matching
        a prefix of 'b' that is at least 'min_length'
        characters long.  If no such overlap exists,
        return 0. """
    start = 0  # start all the way at the left
    while True:
        start = a.find(b[:min_length], start)  # look for b's suffx in a
        if start == -1:  # no more occurrences to right
            return 0
        # found occurrence; check for full suffix/prefix match
        if b.startswith(a[start:]):
            return len(a)-start
        start += 1  # move just past previous match

def scs(ss):
    """ Returns shortest common superstring of given
        strings, which must be the same length """
    shortest_sup = None
    listCandidates = set()
    for ssperm in itertools.permutations(ss):
        sup = ssperm[0]  # superstring starts as first string
        for i in range(len(ss)-1):
            # overlap adjacent strings A and B in the permutation
            olen = overlap(ssperm[i], ssperm[i+1], min_length=1)
            # add non-overlapping portion of B to superstring
            sup += ssperm[i+1][olen:]
        if shortest_sup is None or len(sup) < len(shortest_sup):
            shortest_sup = sup  # found shorter superstring
            # because is new, reset set
            listCandidates = set()
            listCandidates.add(sup)
        # check for other candidates
        if len(shortest_sup) == len(sup):
            listCandidates.add(sup)
    return shortest_sup, listCandidates  # return shortest

def assemble(reads,k=3):
    """Returns length of pair of reads with a minimum overlap of k bases"""
    # create dictionary with sets of kmers
    kmerD = dict()
    SetOlaps = set()
    SetTest = set()
    listSeqs = []
    read01, read02 = None, None
    bestOlap = 0
    for i in range(len(reads)):
        for j in range(len(reads[i])-k+1):
            kmerName = reads[i][j:k+j]
            # update dictionary
            try:
                kmerD[kmerName].add(reads[i])
            except KeyError:
                kmerD[kmerName] = {reads[i]}
    for i in reads:
        suffix = i[-k:]
        suffixDict = kmerD[suffix]
        for x in suffixDict:
            if i != x:
                # tempSet = kmerD.get(suffix)
                olapVal = overlap(i,x,k)
                if olapVal > bestOlap:
                    read01, read02 = i, x
                    bestOlap = olapVal
                    # SetOlaps.update([(i,x)])
                    # NewSeq = "".join(i+x[olapVal:])
                    # SetTest.update([NewSeq])
            # longest Seq for each read
        # print()
        # if len(SetTest) > 0:
        #     listSeqs.append(max(SetTest, key=len))
        # SetTest = set()
    return read01, read02, bestOlap

def greedyscs(reads, k = 0):
    """Return a candidate for shortest common superstring"""
    read01, read02 , olen = assemble(reads,k)
    while olen > 0:
        # replace old reads with overlap read
        reads.remove(read01)
        reads.remove(read02)
        # add overlap read
        reads.append(read01+read02[olen:])
        read01, read02 , olen = assemble(reads,k)
    return "".join(reads)
seqs,_ = readfastq(FileName)
# newlist = assemble(reads,30)
a = greedyscs(seqs,10)
print("stop")
print(a)
# strings = ["CCT", "CTT", "TGC", "TGG", "GAT", "ATT"]
# a,b = scs(strings)
# print("SCS:",a, ", lenght:",len(a))
# print("SCS candidates:",b,", lenght:",len(b))
# 