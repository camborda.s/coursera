from itertools import permutations
def overlap(a, b, min_length=3):
    """Returns length of longest suffix of a matching
    a prefix of b with a min length. Otherwise, it
    returns 0 """
    start = 0  # start all the way at the left
    while True:
        start = a.find(b[:min_length], start)  # look for b's prefix in a
        if start == -1:  # no more occurrences to right
            return 0
        # found occurrence; check for full suffix/prefix match
        if b.startswith(a[start:]):
            return len(a)-start
        start += 1  # move just past previous match
def scs(setStrings):
    """Returns the shortest common superstring in a set of string"""
    scs = None
    # check in every permutation for its length
    for perm in permutations(setStrings):
        # first one is index 0
        sup = perm[0]
        # -1 since we have already a candidate
        for i in range(len(setStrings)-1):
            # min has to be 1 (minimal overlap)
            olen = overlap(perm[i],perm[i+1],1)
            sup += perm[i+1][olen:]
        # if sup is shorter then we found a new candidate
        if scs == None or len(sup) < len(scs):
            scs = sup
    return scs

def pickMaxOLap(reads,k):
    """Return strings with the maximum overlap k """
    read01, read02 = None, None
    bestOlap = 0
    # for permutations (per 2) check the overlapLenght
    for a, b in permutations(reads,2):
        olen = overlap(a,b,k)
        # if larger, replace
        if olen > bestOlap:
            read01, read02 = a, b
            bestOlap = olen
    return read01, read02, bestOlap
def greedyscs(reads, k = 0):
    """Return a candidate for shortest common superstring"""
    read01, read02 , olen = pickMaxOLap(reads,k)
    while olen > 0:
        # replace old reads with overlap read
        reads.remove(read01)
        reads.remove(read02)
        # add overlap read
        reads.append(read01+read02[olen:])
        read01, read02 , olen = pickMaxOLap(reads,k)
    return "".join(reads)

def deBrujin(string,k):
    """For each k-mer in the string, it take (k-1)-mers and creates a
    edge between both or create nodes """
    edge = []
    nodes = set()
    # last position a kmer could initate
    for i in range(len(string)-k+1):
        edge.append((string[i:i+k-1],string[i+1:i+k]))
        nodes.add(string[i:i+k-1])
        nodes.add(string[i+1:i+k])
    return edge, nodes
# a, b = deBrujin("ASEREJEPORDOS",3)
# print(b)

ListString = ["AAA","AAB","ABA","ABB","BAA","BAB","BBA","BBB"]
# a = scs(ListString)
a,b,c = pickMaxOLap(ListString,1)
d = greedyscs(ListString,2)
print(a,b,c)
print(d)