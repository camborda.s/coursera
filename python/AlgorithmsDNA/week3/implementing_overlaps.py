from itertools import permutations
def overlap(a,b,min=3):
    """Return length of longest Suffix of a in prefix of b
    that is at least min of 3 characters long. Otherwise
    returns 0"""
    start = 0
    while True:
        # searches for b suffix in a
        start = a.find(b[:min],start)
        if start == -1:
            return 0
        # if found occurrence, check for full suffix/prefix
        if b.startswith(a[start:]):
            return len(a)-start
        # otherwise, move past previous match 
        start += 1
# print(overlap("ATGCGCGT","CGTCGTAGG"))
def naiveOverlap(reads, k):
    """Return list of overlaps between reads with a
    minimum overlap of length k"""
    olap = {}
    # check in possibilities of permutations
    for a, b in permutations(reads,2):
        # overlap length
        olen = overlap(a, b, k)
        if olen > 0:
            olap[(a,b)] = olen
    return olap
# reads = ["ACGGATGATC","GATCAAGT","TTCACGGA"]
# print(naiveOverlap(reads,3))
# # print(list(permutations(reads,2)))