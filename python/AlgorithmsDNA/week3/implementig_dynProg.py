def editDistance(x,y):
    """Compute the edit distance between x and y"""
    #create matrix with edit distance of the substrings of both variables
    D=[]
    # x+1 by y+1 (+1 is due empty strings)
    #First loop is to create the 0's in the matrix
    #second and third loop is always i -> empty = editDist
    for i in range(len(x)+1):
        D.append([0]*(len(y)+1))
    for i in range(len(x)+1):
        D[i][0]=i
    for i in range(len(y)+1):
        D[0][i]=i
    #fill the rest of the matrix
    for i in range(1, len(x)+1):
        for j in range(1, len(y)+1):
            # +1 -> according to the formula d(x,y) = 1
            distHor = D[i][j-1]+1
            distVer = D[i-1][j]+1
            #Check is Substrings are the same
            #else, penalty +1 (like empty)
            if x[i-1] == y[j-1]:
                distDia = D[i-1][j-1]
            else:
                distDia = D[i-1][j-1] +1
            distmin = min(distDia, distHor,distVer)
            D[i][j] = distmin
    print("Edit distance:", D[-1][-1])
    return
def penaltyMatrix():
    """creates a penalty matrix. Same nucleotide gets a penalty of 0, same purine or pyrimidine bases receive 4 and empty strings 0"""
    alphabet = ["A","C","G","T",""]
    # dictionary for penalties of 2
    penalty2 = {"A":"G","T":"C","C":"T","G":"A","":""}
    penD = []
    #create matrix of 0's
    for i in range(len(alphabet)):
        penD.append([0]*(len(alphabet)))
    # penalty scores
    for i in range(len(alphabet)):
        for j in range(len(alphabet)):
            penalty = 0
            if alphabet[j] == alphabet[i]:
                penalty = 0
            elif penalty2[alphabet[j]] ==  alphabet[i]:
                penalty = 2
            elif alphabet[j] == "" or alphabet[i] == "":
                penalty = 8
            else:
                penalty = 4
            if alphabet[i] and alphabet[j] == "":
                penalty = 8
            penD[i][j] = penalty
    return penD, alphabet

def globalAlignment(x,y):
    """Compute the global edit distance between x and y"""
    #create matrix with edit distance of the substrings of both variables
    penD, alphabet = penaltyMatrix()
    alphabet = ["A","C","G","T"]
    D=[]
    # x+1 by y+1 (+1 is due empty strings)
    #First loop is to create the 0's in the matrix
    #second and third loop is always i -> empty = editDist
    for i in range(len(x)+1):
        D.append([0]*(len(y)+1))

    for i in range(1, len(x)+1):
        D[i][0] = D[i-1][0] + penD[alphabet.index(x[i-1])][-1]
    for i in range(1, len(y)+1):
        D[0][i] = D[0][i-1] + penD[-1][alphabet.index(y[i-1])]
        
    #fill the rest of the matrix
    for i in range(1, len(x)+1):
        for j in range(1, len(y)+1):
            # +1 -> according to the formula d(x,y) = 1
            distHor = D[i][j-1] + penD[-1][alphabet.index(y[j-1])]
            distVer = D[i-1][j] + penD[alphabet.index(x[i-1])][-1]
            #Check is Substrings are the same
            #else, penalty +1 (like empty)
            if x[i-1] == y[j-1]:
                distDia = D[i-1][j-1]
            else:
                distDia = D[i-1][j-1] + penD[alphabet.index(x[i-1])][alphabet.index(y[j-1])]
            distmin = min(distDia, distHor,distVer)
            D[i][j] = distmin
    print("Global edit distance:", D[-1][-1])
    return
# globalAlignment("TACCAGATTCGA","TACCAAATTGA")
