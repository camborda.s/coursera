"""adapt the editDistance function we saw in practical
 (copied below) to answer questions 1 and 2 below. Your 
 function should take arguments p (pattern), t (text) and
  should return the edit distance of the match between P 
  and T with the fewest edits."""
#Hint: In the "A new solution to approximate matching" video
#  we saw that the best approximate match of 
# P =GCGTATG Cwithin T =TATTGGCTATACGGTT had 2 edits. You can
#  use this and other small examples to double-check that your 
# function is working.
from pathlib import Path
from itertools import permutations
DirName = Path.cwd().joinpath("coursera").joinpath("python").joinpath("AlgorithmsDNA").joinpath("week3")
FileName = DirName.joinpath("ERR266411_1.for_asm.fastq")
def readGenome(filename):
    """Returns string of bases of filename"""
    genome = ""
    with open(filename ,"r") as f:
        for line in f:
            if not line[0] == ">":
                #If line then and the like to 
                genome += line.rstrip()
    return genome

def readfastq(filename):
    """Returns the reads and qualities of a filename with a fastq format"""
    sequences = []
    qualities = []
    with open(filename) as fh:
        while True:
            fh.readline()
            seq = fh.readline().rstrip()
            fh.readline()
            qual = fh.readline().rstrip()
            if len(seq) == 0:
                break
            sequences.append(seq)
            qualities.append(qual)
    return sequences, qualities

def editDistanceInString(x, y):
    # Create distance matrix
    D = []
    for i in range(len(x)+1):
        D.append([0]*(len(y)+1))
    # Initialize first row and column of matrix
    for i in range(len(x)+1):
        D[i][0] = i
    for i in range(len(y)+1):
        D[0][i] = 0
    # Fill in the rest of the matrix
    for i in range(1, len(x)+1):
        for j in range(1, len(y)+1):
            distHor = D[i][j-1] + 1
            distVer = D[i-1][j] + 1
            if x[i-1] == y[j-1]:
                distDiag = D[i-1][j-1]
            else:
                distDiag = D[i-1][j-1] + 1
            D[i][j] = min(distHor, distVer, distDiag)
    # Edit distance is the minimun value of the last row
    minvalue = [min(D[-1])]
    return minvalue

# genome = readGenome(FileName)
# min = editDistanceInString("GATTTACCAGATTGAG",genome)
# print(min)
"""Next, find all pairs of reads with an exact suffix/prefix match of
 length at least 30. Don't overlap a read with itself; if a read has a
 suffix/prefix match to itself, ignore that match. Ignore reverse complements."""

def overlap(a, b, min_length=3):
    start = 0  # start all the way at the left
    while True:
        start = a.find(b[:min_length], start)  # look for b's prefix in a
        if start == -1:  # no more occurrences to right
            return 0
        # found occurrence; check for full suffix/prefix match
        if b.startswith(a[start:]):
            return len(a)-start
        start += 1  # move just past previous match
def naiveOverlap(reads, k):
    """Return list of overlaps between reads with a
    minimum overlap of length k"""
    olap = {}
    # check in possibilities of permutations
    for a, b in permutations(reads,2):
        # overlap length
        olen = overlap(a, b, k)
        if olen > 0:
            olap[(a,b)] = olen
    return olap

def overlapPairs(reads,k=3):
    """Returns length of pair of reads with a minimum overlap of k bases"""
    # create dictionary with sets of kmers
    kmerD = dict()
    for i in range(len(reads)):
        for j in range(len(reads[i])-k+1):
            kmerName = reads[i][j:k+j]
            # update dictionary
            try:
                kmerD[kmerName].add(reads[i])
            except KeyError:
                kmerD[kmerName] = {reads[i]}
    # create set with overlaps to avoid repetitions
    SetOlaps = set()
    # count mount of reads that has a suffix in the
    CheckRead = set()
    # compare suffix with prefix of reads
    for read in reads:
        suffix = read[-k:]
        # if suffix is in the dict then we can use those reads
        suffixDict = kmerD[suffix]
        # print("O")
        # check in list, if not, check overlap
        for x in suffixDict:
            if read != x:
                # tempSet = kmerD.get(suffix)
                olapVal = overlap(read,x,k)
                # SetOlaps.update(olap)
                if olapVal > 0:
                    SetOlaps.update([(read,x)])
                    # Set to avoid duplicates
                    CheckRead.update([read])
    return len(SetOlaps),SetOlaps, len(CheckRead)

reads, _ = readfastq(FileName)
a, b, c= overlapPairs(reads,30)
print(a,c)