import bisect
from pathlib import Path
# DirName = Path.cwd.joinpath("coursera").joinpath.("AlgorithmsDNA").joinpath("week2")
__supervisor__ = "Ben Langmead"
class Index(object):
    #k-mer lenght = k
    def __init__(self,t,k):
        """creates the kmers of string t"""
        self.k= k
        self.index =[]
        #every indxe but does not pass the end text
        for i in range(len(t)-k+1):
            self.index.append((t[i:i+k],i))
        self.index.sort()
    
    def query(self, p):
        """ Return index hits for first k-mer of p """
        kmer = p[:self.k]
        #  -1 , all indexes have to be greater than -1, we will always get the first occurence
        i= bisect.bisect_left(self.index, (kmer, -1))
        hits =[]
        while  i < len(self.index):
            #If kmer is not in the kmer-list (of t)
            if self.index[i][0] != kmer:
                break
            #otherwise it is a hit. I hit is A POSSIBLY place where p could start
            hits.append(self.index[i][1])
            i+=1
        return hits

    def queryIndex(p,t,index):
        """ Return list of offsets where matches occurred """
        k = index.k
        # list of offsets 
        offsets = []
        for i in index.query(p):
            #if occurence
            if p[k:] == t[i+k:i+len(p)]:
                offsets.append(i)
        print("Offset-list:",offsets)
        return offsets

t = "GCTACGATCTAGAATCTACCCCCCCCCCTCTACCCCCCCCCCCCCCCCCCC"
p = "TCTA"

indexDeT = Index(t,3)
Index.queryIndex(p,t,indexDeT)
