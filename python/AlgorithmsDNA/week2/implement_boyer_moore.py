import sys
from pathlib import Path
DirName = Path.cwd().joinpath("coursera").joinpath("python").joinpath("AlgorithmsDNA").joinpath("week2")
import bm_preproc   
__supervisor__ = "Ben Langmead"
def boyer_moore(p,t,p_bm):
    """Preprocess given pattern and invocate Boyer-Moore matching"""
    i = 0
    occurrences=[]
    while i < len(t)-len(p)+1:
        #the amount to be moved along
        shift=1
        mismatched = False
        # the last -1  means reverse
        # The second -1 makes us stop at 0
        for j in range(len(p)-1,-1,-1):
            if not p[j]== t[i+j]:
                skip_bc = p_bm.bad_character_rule(j,t[i+j])
                skip_gs = p_bm.good_suffix_rule(j)
                shift = max(skip_bc,skip_gs,shift)
                mismatched = True
                break
        if not mismatched:
            occurrences.append(i)
            skip_gs=p_bm.match_skip()
            shift= max(shift,skip_gs)
        i+=shift
    return occurrences

# print(boyer_moore("TCTA","GCTACGATCTAGAATCTA"))