# from pathlib import Path
import bm_preproc
from implement_boyer_moore import boyer_moore
__supervisor__ = "Ben Langmead"
def approximate_match(p,t,m):
    """P = pattern, t = text to be searched in, m is the maximum number of mismatches"""
    #seq_length is the length for the substrings of p (in this case there are m+1)
    seg_length = int(round(len(p) / (m+1)))
    #fill with indexes of all matches
    all_matches = set()
    for i in range(m+1):
        start = i*seg_length
        # min is there in order to prevent overcoming the indexes
        end = min((i+1)*seg_length, len(p))
        # creates BoyerMoore object
        p_bm = bm_preproc.BoyerMoore(p=p[start:end], alphabet="ACGT")
        matches = boyer_moore(p=p[start:end], p_bm=p_bm, t=t)
        for m in matches:
            if m < start or m-start+len(p) > len(t):
                #skip loop
                continue
            mismatches = 0
            #comparing up to beginning of start
            for j in range(0,start):
                if not p[j] == t[m-start+j]:
                    mismatches += 1
                    if mismatches > m:
                        break
            # comparing sufix
            for j in range(end, len(p)):
                if not p[j] == t[m-start+j]:
                    mismatches += 1
                    if mismatches > m:
                        break
            if mismatches <= m:
                all_matches.add(m - start)
    return list(all_matches)
p = 'CACACATTT'
t = 'CACACATTTCACAGATTTCACAGGTTTCACACAGGGCAGACGTTTCAGACGTTG'
print(approximate_match(p=p,t=t,m=2))
