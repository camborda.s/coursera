import bm_preproc as bm
from pathlib import Path
import kmer_index as km
import implement_boyer_moore as im
dirName = Path.cwd().joinpath("coursera").joinpath("python").joinpath("AlgorithmsDNA").joinpath("week2")
fileName = dirName.joinpath("chr1.GRCh38.excerpt.fasta")
# Implement versions of the naive exact matching and Boyer-Moore algorithms that additionally count and return (a) the number of character comparisons performed and I(b) the number of alignments tried. Roughly speaking, these measure how much work the two different algorithms are doing.
class homework:
    def readgenome(filename):
        """Reads filename and return a string with the sequence from the file"""
        genome =""
        with open(filename ,"r") as f:
            for line in f:
                if not line[0] == ">":
                    #If line then and the like to 
                    genome += line.rstrip()
        return genome

    def readfastq(filename):
        sequences = []
        qualities = []
        with open(filename) as fh:
            while True:
                fh.readline()
                seq = fh.readline().rstrip()
                fh.readline()
                qual = fh.readline().rstrip()
                if len(seq) == 0:
                    break
                sequences.append(seq)
                qualities.append(qual)
        return sequences, qualities

def naive(p,t,m):
    """Aligns the string p in reference string t and returns their positions if available. m represens the maximum number of mismatches"""
    occurrence = []
    charCount = 0
    #Alignment count
    for i in range(len(t)-len(p)+1):
        mismatches = 0
        #character count
        for j in range(len(p)):
            charCount += 1
            if not t[i+j] == p[j]:
                mismatches += 1
                break
        if mismatches <= m:
            occurrence.append(i)
            # charCount -= len(p)
    print("Number of occurrences:", len(occurrence))
    print("Locations", occurrence)
    print("Number of alignments comparison:", i+1)
    print("Number of character comparison:", charCount)
    return occurrence

def boyer_moore(p,t,p_bm):
    """Preprocess given pattern and invocate Boyer-Moore matching"""
    i = 0
    occurrences = []
    charCount = 0
    alignCount = 0
    while i < len(t)-len(p)+1:
        #the amount to be moved along
        shift = 1
        mismatched = False
        alignCount += 1
        # the last -1  means reverse
        # The second -1 makes us stop at 0
        for j in range(len(p)-1, -1, -1):
            charCount += 1
            if not p[j] == t[i+j]:
                skip_bc = p_bm.bad_character_rule(j, t[i+j])
                skip_gs = p_bm.good_suffix_rule(j)
                shift = max(skip_bc, skip_gs, shift)
                mismatched = True
                break
        if not mismatched:
            occurrences.append(i)
            skip_gs = p_bm.match_skip()
            shift = max(shift, skip_gs)
        i += shift
    print("Number of occurrences:", len(occurrences))
    print("Locations", occurrences)
    print("Number of alignments comparison:", alignCount)
    print("Number of character comparison:", charCount)
    return occurrences

def queryIndex(p,t,index,mm):
    """ Return list of offsets where matches occurred """
    k = index.k
    # list of offsets 
    seg_number = mm+1
    seg_length = int(round((len(p)-k) / (seg_number)))
    if seg_length == 1:
        print("Seq cortas")
        return
    offsets = []
    #index.query are hits
    hits = index.query(p)
    for i in index.query(p):
        missmatch = 0
        # print("Searching in offset",i)
        for x in range(seg_number+1):
        #start de sequence para t
        # +k is because we have already a hit list
            start = k+(x*seg_length) # correcto
            end = min(k+(x+1)*seg_length, len(p))
            # print(p[start:end],"with T:",t[i+start:i+end])
            if len(p[start:end]) == len(t[i+start:i+end]):
                # print(p[start:end],"with T:",t[i+start:i+end])
                for n in range(len(p[start:end])):
                    # print("2)",p[start+n],"with T:",t[i+start+n])
                    if p[start+n] == t[i+start+n]:
                        # print("They are the same")
                        pass
                    else:
                        missmatch +=1
                    if missmatch > mm:
                        # print("Too much missmatches")
                        break
            else:
                # print("Not the same length")
                missmatch = mm+1
                break
        # print("Total missmatches:",missmatch)
        if missmatch <= mm:
            offsets.append(i)
    print("Offset-list:",offsets)
    print("Length:",len(offsets))
    return offsets
def naive_2mm(p,t):
    """Aligns the string p in reference string t with a tolerance of maximal 2 missmatches and returns its positions if available"""
    occurrence = list()
    for i in range(len(t)-len(p)+1):
        #j is index of p | i+j is sequence
        matchCount = 0
        for j in range(len(p)):
            if t[j+i] ==p[j]:
                # print(t[i+j],p[j],"Match")
                matchCount +=1
            else:
                # print("No match")
                pass
        if matchCount >=(len(p)-2):
            # print(matchCount,"Solo tiene maximo 2 errores")
            occurrence.append(i)
    print("Offset-list:",occurrence)
    print("Length:",len(occurrence))
    return occurrence
def approximate_match(p,t,m):
    """P = pattern, t = text to be searched in, m is the maximum number of mismatches"""
    #seq_length is the length for the substrings of p (in this case there are m+1)
    seg_length = int(round(len(p) / (m+1)))
    #fill with indexes of all matches
    all_matches = set()
    for i in range(m+1):
        start = i*seg_length
        # min is there in order to prevent overcoming the indexes
        end = min((i+1)*seg_length, len(p))
        # creates BoyerMoore object
        p_bm = bm.BoyerMoore(p=p[start:end], alphabet="ACGT")
        matches = im.boyer_moore(p=p[start:end], p_bm=p_bm, t=t)
        for m in matches:
            if m < start or m-start+len(p) > len(t):
                #skip loop
                continue
            mismatches = 0
            #comparing up to beginning of start
            for j in range(0,start):
                if not p[j] == t[m-start+j]:
                    mismatches += 1
                    if mismatches > m:
                        break
            # comparing sufix
            for j in range(end, len(p)):
                if not p[j] == t[m-start+j]:
                    mismatches += 1
                    if mismatches > m:
                        break
            if mismatches <= m:
                all_matches.add(m - start)
    print("List:",sorted(all_matches))
    print("lenght:",len(all_matches))
    return
p ="GGCGCGGTGGCTCACGCCTGTAAT"
sequence  = homework.readgenome(fileName)
# p_bm = bm.BoyerMoore(p, alphabet="ACGT")
# boyer_moore(p,sequence,p_bm)
approximate_match(p=p,t=sequence,m=2)
print("----------------")
indexDeT = km.Index(sequence,8)
queryIndex(p,sequence,indexDeT,2)
print("Naive 2mm")
naive_2mm(p,sequence)
