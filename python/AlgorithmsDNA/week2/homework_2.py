# Implement the pigeonhole principle using \verb|Index|Index to find exact matches for the partitions. Assume P always has length 24, and that we are looking for approximate matches with up to 2 mismatches (substitutions). We will use an 8-mer index.

# Write a function that, given a length-24 pattern P and given an \verb|Index|Index object built on 8-mers, finds all approximate occurrences of P within T with up to 2 mismatches. Insertions and deletions are not allowed. Don't consider any reverse complements.

# Hint 1: Multiple index hits might direct you to the same match multiple times, but be careful not to count a match more than once.

# Hint 2: You can check your work by comparing the output of your new function to that of the \verb|naive_2mm|naive_2mm function implemented in the previous module.

import kmer_index as km

def queryIndex(p,t,index,mm):
        """ Return list of offsets where matches occurred """
        k = index.k
        # list of offsets 
        seg_number = mm+1
        seg_length = int(round((len(p)-k) / (seg_number)))
        if seg_length == 1:
            print("Seq cortas")
            return
        offsets = []
        #index.query are hits
        hits = index.query(p)
        for i in index.query(p):
            missmatch = 0
            # print("Searching in offset",i)
            for x in range(seg_number+1):
            #start de sequence para t
            # +k is because we have already a hit list
                start = k+(x*seg_length) # correcto
                end = min(k+(x+1)*seg_length, len(p))
                # print(p[start:end],"with T:",t[i+start:i+end])
                if len(p[start:end]) == len(t[i+start:i+end]):
                    # print(p[start:end],"with T:",t[i+start:i+end])
                    for n in range(len(p[start:end])):
                        # print("2)",p[start+n],"with T:",t[i+start+n])
                        if p[start+n] == t[i+start+n]:
                            # print("They are the same")
                            pass
                        else:
                            missmatch +=1
                        if missmatch > mm:
                            # print("Too much missmatches")
                            break
                else:
                    # print("Not the same length")
                    missmatch = mm+1
                    break
            # print("Total missmatches:",missmatch)
            if missmatch <= mm:
                offsets.append(i)
        print("Offset-list:",offsets)
        return offsets

p = 'CACACATTT'
t = 'CACACATTT CACAGATTT CACAGGTTT CACACAGGG CAGACGTTT CAGACGTTG'
# needle need noodle needle
# p="ACTTTA"
# t="ACTTTA ACTTTC ACTTCC ACTCCC ACCCCC"
indexDeT = km.Index(t,4)
queryIndex(p,t,indexDeT,2)
