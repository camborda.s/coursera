from pathlib import Path
from Bio.Blast import NCBIWWW, NCBIXML
from Bio.Seq import Seq
dirname=Path.cwd().joinpath("coursera").joinpath("python")
# Runing Blast
fasta_string = open(str(dirname.joinpath("seqtest.fa"))).read()
# fasta_string = open(str(dirname.joinpath("m_cold.fa"))).read()
# fasta_string2 = open(str(dirname.joinpath("week4_unknownseq.fa"))).read()
result_handle = NCBIWWW.qblast("blastn","nt",fasta_string)
# result_handle2 = NCBIWWW.qblast("blastn","nt",fasta_string2)
# help(Bio.Blast.NCBIWWW.qblast)
# Exporting XML files
# blast_record1 = NCBIXML.read(result_handle)
# help(NCBIXML)
len(blast_record1.alignments)
EValue = 0.01
for alignment in blast_record1.alignments:
    for hsp in alignment.hsps:
        if hsp.expect < EValue:
            print("****Alignment*****")
            print("sequence:",alignment.title)
            print("length:",alignment.length)
            print("e value",hsp.expect)
            print(hsp.query)
help(Seq)
my_seq = Seq("CGGTACGCTTATGTCACGTAG*AAAAAA")
# print('reverse complement is %s' % complement(my_seq))
# print('reverse complement is %s' % reverse(my_seq.complement()))
print('reverse complement is %s' % my_seq.reverse_complement())

my_seq2 =Seq("""TGGGCCTCATATTTATCCTATATACCATGTTCGTATGGTGGCGCGATGTTCTACGTGAATCCACGTTCGAAGGACATCATACCAAAGTCGTACAATTAGGACCTCGATATGGTTTTATTCTGTTTATCGTATCGGAGGTTATGTTCTTTTTTGCTCTTTTTCGGGCTTCTTCTCATTCTTCTTTGGCACCTACGGTAGAG""")
my_seq2.translate()